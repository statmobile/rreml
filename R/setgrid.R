"setgrid" <-
function(n,dist)
  {
    points <- NULL
    for (i in 1:n)
      {
        for (j in 1:n)
          {
            point <- c(dist*(i-1),dist*(j-1))
            points <- rbind(points,point)
            rm(point)
          }
      }

    ## Now compute distances
    euc.dist <- function(p1,p2)
      {
        dist <- sqrt(t(p1-p2)%*%(p1-p2))
        dist
      }

    distances <- matrix(rep(0,n^4),nrow=n^2)
    for (i in 1:n^2)
      {
        for (j in i:n^2)
          {
            distances[i,j] <- euc.dist(points[i,],points[j,])
          }
      }
    distances <- distances+t(distances)
         
          
    return(list(points=points,D=distances))
  }

