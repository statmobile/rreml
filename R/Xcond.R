## Function to set n x p X based on condtion number c
"Xcond" <- function(n,p,c,scale=0,seed=0)
  {
    ## Set seed if it is used
    if(seed>0) set.seed(seed)
    m <- min(n,p)
    tol <- 1e-6
    ## Set max and min singular values
    sing.max <- 1
    sing.min <- sing.max/c

    ## Now create order of singular values
    d <- seq(sing.min,sing.max,length.out=m)

    ## generate orthonormal vectors for X^TX
    ## Using gram-schmidt on random columnvectors
    T <- matrix(0,nrow=n,ncol=m)
    t.col <- runif(n)
    T[,1] <- t.col/drop(sqrt(t(t.col)%*%t.col))
    for (i in 1:m)
      {
        nrm <- 0
        while (nrm<tol)
          {
            vi <- runif(n)
            vi <- vi-T[,1:(i-1)]%*%(t(T[,1:(i-1)])%*%vi)
            nrm <- sqrt(t(t.col)%*%t.col)
          }
        T[,i] <- vi/drop(sqrt(t(vi)%*%vi))
      }

    ## generate orthonormal vectors for XX^T
    ## Using gram-schmidt on random columnvectors
    V <- matrix(0,nrow=p,ncol=m)
    t.col <- runif(p)
    V[,1] <- t.col/drop(sqrt(t(t.col)%*%t.col))
    for (i in 1:m)
      {
        nrm <- 0
        while (nrm<tol)
          {
            vi <- runif(p)
            vi <- vi-V[,1:(i-1)]%*%(t(V[,1:(i-1)])%*%vi)
            nrm <- sqrt(t(t.col)%*%t.col)
          }
        V[,i] <- vi/drop(sqrt(t(vi)%*%vi))
      }

    ##Now use SVD Theorem to produce X
    X <- T%*%diag(d)%*%t(V)

    ## If scaling is desired, scale X
    ## Note this could skew condition number
    if (scale!=0)
    {
        ## Look into just using the scale() function in R
        
        ## Now set colums to sum to zero
        Xmean <- colMeans(X)
        X <- X-rep(Xmean,rep(n,p))

        Xscale <- drop(rep(1/scale,n)%*%X^2)^.5 # Scale to scale diagonal

        X <- X/rep(Xscale,rep(n,p)) # Now set diagonal of X^TX
    }

    return(X)

}

