"run.example" <-
function(X,D,theta,variog=c("exponential","gaussian"))
  {
    ## Set Q for parameters
    variog <- match.arg(variog)

    if (variog=="exponential" || variog=="gaussian")
      {
        ## Number of spatial parmeters
        p <- 2
      }
    Q <- matrix(rep(NA,p*p),nrow=p)
    for (i in 1:p){
      for (j in 1:p){
        qij <- setqij(i,j,theta,X,D,method=variog)
        Q[i,j] <- qij
      }}
    Qeigen <- eigen(Q,only.values=TRUE)$values
    H <- matrix(rep(NA,4),nrow=2)
    for (i in 1:p){
      for (j in 1:p){
        V <- setV(theta,D,method=variog)
        dVi <- setdV(i,theta,D,method=variog)
        dVj <- setdV(j,theta,D,method=variog)
        W <- setW(0,solve(V),X)
        hij <- sethij0(dVi,dVj,W)
        H[i,j] <- hij
      }
    }
    deriv0 <- sum(diag(solve(H)%*%Q%*%solve(H)))
    return(list(Qeigen=Qeigen,Q=Q,H=H,deriv0=deriv0))
  }

