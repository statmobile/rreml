"setV" <-
function(theta,d,method=c("exponential","gaussian","matern"))
{
    method <- match.arg(method)

    if (method=="exponential")
    {
        set.exp <- function(dij)
        {
            (1-theta[1])*exp(-dij/theta[2])
        }
        V <- set.exp(d)
        diag(V) <- rep(1,dim(d)[1])
    }
    if (method=="gaussian")
    {
        set.gaus <- function(dij)
        {
            (1-theta[1])*exp(-dij^2/theta[2]^2)
        }
        V <- set.gaus(d)
        diag(V) <- rep(1,dim(d)[1])
    }
    if (method=="matern")
    {
        ## Make sure this matches C code in spatCOV
        set.mat <- function(dij)
        {
            theta1 <- theta[1]; theta2 <- theta[2]
            t <- 2*sqrt(theta2)*dij/theta1
            t^theta2*besselK(t,theta2)/(2^(theta2-1)*gamma(theta2))
        }
        V <- set.mat(d)
        diag(V) <- rep(1,dim(d)[1])
    }



    return(V)
  }

