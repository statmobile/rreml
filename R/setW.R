"setW" <- function(lambda,Vinv,X,Sigmainv=diag(rep(1,dim(X)[2])))
{
    XtV <- solve(t(X)%*%Vinv%*%X+lambda*Sigmainv)
    return(Vinv-Vinv%*%X%*%XtV%*%t(X)%*%Vinv)
}

