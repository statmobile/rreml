"spatV" <- function(pars,coords,vars,model=1,nug=TRUE,
                    Dscale=1,blockstarts=c(1))
{
    CsetV <- function(coordsvars,pars,model,nug,Dscale)
    {
        coords <- coordsvars[[1]]
        vars <- coordsvars[[2]]
        if(dim(coords)[1]!=length(vars)) stop("CsetV dims of coords and vars don't match")
        .Call("setV",
              as.double(pars),
              as.integer(model),
              nug,
              coords,
              as.double(vars),
              as.double(Dscale),
              PACKAGE="rreml")
    }
    if(length(blockstarts)!=1)
    {
        blockstops <- c(blockstarts[-1]-1,dim(coords)[1])
        coordsvars <- lapply(1:length(blockstarts),function(i){
                             list(coords[blockstarts[i]:blockstops[i],],
                                  vars[blockstarts[i]:blockstops[i]])})
        V <- lapply(coordsvars,CsetV,pars,model,nug,Dscale)
    }
    else V <- CsetV(list(coords,vars),pars,model,nug,Dscale)
    V
}
