\name{setb}
\alias{setb}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ ~~function to do ... ~~ }
\description{
  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
setb(theta, lambda, beta, d, X, spat.method = c("exponential", "gaussian", "matern"))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{theta}{ ~~Describe \code{theta} here~~ }
  \item{lambda}{ ~~Describe \code{lambda} here~~ }
  \item{beta}{ ~~Describe \code{beta} here~~ }
  \item{d}{ ~~Describe \code{d} here~~ }
  \item{X}{ ~~Describe \code{X} here~~ }
  \item{spat.method}{ ~~Describe \code{spat.method} here~~ }
}
\details{
  ~~ If necessary, more details than the description above ~~
}
\value{
  ~Describe the value returned
  If it is a LIST, use
  \item{comp1 }{Description of 'comp1'}
  \item{comp2 }{Description of 'comp2'}
  ...
}
\references{ ~put references to the literature/web site here ~ }
\author{ ~~who you are~~ }
\note{ ~~further notes~~ 
}
\seealso{ ~~objects to See Also as \code{\link{help}}, ~~~ }
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (theta, lambda, beta, d, X, spat.method = c("exponential", 
    "gaussian", "matern")) 
{
    spat.method <- match.arg(spat.method)
    p <- length(theta)
    b <- rep(0, p)
    for (i in 1:p) {
        b[i] <- setbi(i, theta, lambda, beta, d, X, spat.method)
    }
    return(b)
  }
}
\keyword{ models }
\keyword{ regression }

