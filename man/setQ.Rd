\name{setQ}
\alias{setQ}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ Set Q }
\description{
  Set Q
}
\usage{
setQ(theta, X, d, method = c("exponential", "gaussian", "matern"))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{theta}{ Spatial Parameters }
  \item{X}{ Design Matrix }
  \item{d}{ Distance Matrix }
  \item{method}{ Which Variogram Model to Use }
}
\details{
  Recall that each component of Q relies greatly on which row and column
  it is.
}
\value{
}
\references{ Dissertation }
\author{ Brian J. Lopes }
\note{
}
\seealso{
  \code{\link{setR}},\code{\link{run.example}},\code{\link{setW}},\code{\link{setV}},\code{\link{setdV}},\code{\link{setd2V}}  
}
\examples{
}
\keyword{ models }
\keyword{ regression }
