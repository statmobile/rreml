\name{lm.spatrr}
\alias{lm.spatrr}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ Calcuate the spatrr estimate }
\description{
  For a given ridge constant, calculate the estimates of theta and beta.
}
\usage{
lm.spatrr(lambda, Y, X, d, mu, Sigma, spat.method = c("exponential", "gaussian", "matern"),alg=0,scaleX="n")
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{lambda}{ Ridge coefficient }
  \item{Y}{ Observation vector }
  \item{X}{ Design matrix X }
  \item{d}{ Matrix of distances betweeen stations }
  \item{mu}{ Prior mean of beta }
  \item{Sigma}{ Prior covariance of beta }
  \item{spat.method}{ Spatial parameterization to be used }
  \item{alg}{ If 0 use analytical derivative, othewise use numerical
    derivative (default is 0) }
  \item{scaleX}{ What diagonal of \eqn{X^TX} be scaled to (default is "n") }
}
\details{

}
\value{
  Returns a list of theta, the covariance matrix of theta, beta and the
  covariance matrix of beta.
}
\references{ Dissertation }
\author{ Brian J. Lopes }
\note{
}
\seealso{ \code{\link{prof.lkhd.theta}}, \code{\link{dprof.lkhd.theta}} }
\examples{
}
\keyword{ models }
\keyword{ regression }
