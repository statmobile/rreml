\name{setgrid}
\alias{setgrid}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ Set Grid Distances }
\description{
  A quick function to setup stations aligned in a \eqn{n \times n} grid.
}
\usage{
setgrid(n, dist)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{n}{ Number of Rows and Columns }
  \item{dist}{ Distance Between Aligned Stations }
}
\details{
  This will make the distance matrix for \eqn{n^2} stations aligned in an \eqn{n
  \times n} grid where they are aligned dist between each point in the
  grid. 
}
\value{
  Returns a list of the following values:
  \item{statns }{ Coordinates for the \eqn{n^2} stations }
  \item{D }{ Distance matrix for the stations }
}
\references{ Dissertation }
\author{ Brian J. Lopes }
\note{
}
\seealso{ \code{\link{run.example}} }
\examples{
}
\keyword{ models }
\keyword{ regression }

