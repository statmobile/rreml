\name{setH}
\alias{setH}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{ ~~function to do ... ~~ }
\description{
  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
setH(lambda, theta, beta, X, d, method = c("exponential", "gaussian", "matern"))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{lambda}{ ~~Describe \code{lambda} here~~ }
  \item{theta}{ ~~Describe \code{theta} here~~ }
  \item{beta}{ ~~Describe \code{beta} here~~ }
  \item{X}{ ~~Describe \code{X} here~~ }
  \item{d}{ ~~Describe \code{d} here~~ }
  \item{method}{ ~~Describe \code{method} here~~ }
}
\details{
  ~~ If necessary, more details than the description above ~~
}
\value{
  ~Describe the value returned
  If it is a LIST, use
  \item{comp1 }{Description of 'comp1'}
  \item{comp2 }{Description of 'comp2'}
  ...
}
\references{ ~put references to the literature/web site here ~ }
\author{ ~~who you are~~ }
\note{ ~~further notes~~ 
}
\seealso{ ~~objects to See Also as \code{\link{help}}, ~~~ }
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (lambda, theta, beta, X, d, method = c("exponential", 
    "gaussian", "matern")) 
{
    method <- match.arg(method)
    V <- setV(theta, d, method = method)
    Vinv <- solve(V)
    W <- setW(lambda, Vinv, X)
    p <- length(theta)
    H <- matrix(0, nrow = p, ncol = p)
    for (i in 1:p) {
        dVi <- setdV(i, theta, d, method = method)
        for (j in 1:p) {
            dVj <- setdV(j, theta, d, method = method)
            d2V <- setd2V(i, j, theta, d, method = method)
            H[i, j] <- sethij(lambda, theta, beta, X, W, V, Vinv, 
                dVi, dVj, d2V)
        }
    }
    return(H)
  }
}
\keyword{ models }
\keyword{ regression }

