#include "bjl.h"

/*********************************************************
*   This is C code to set the covariance matrix 	 *
*********************************************************/

// d is the distance matrix
// pars is the vector of spatial parameters
// model is the spatial model chosen

SEXP spatCOV(SEXP D, SEXP pars, SEXP model, SEXP nugget)
{
	SEXP V, DIMS;
	int i, j, m, n, npar, mod, nug;
	double var, par1, par2, par3, nugpar; 

	PROTECT(DIMS = getAttrib(D, R_DimSymbol));
	npar = LENGTH(pars);
	m = INTEGER(DIMS)[0];
	n = INTEGER(DIMS)[1];
	if(m != n) error("The distance matrix must be square");

	mod = INTEGER(model)[0];
	nug = INTEGER(nugget)[0];

	if(mod <= 6) //One parameter models 
	{
		if(npar != 2 && nug == 1) error("spatial parameters length is wrong");
		if(npar != 1 && nug == 0) error("spatial parameters length is wrong");
		par1 = REAL(pars)[0];
		if(nug==1) nugpar = REAL(pars)[1];
		else nugpar = 0.0;
		par2 = 0.0; par3 = 0.0;
	}
	if(mod <= 9 && mod > 6) //Two parameter models
	{
		if(npar != 3 && nug == 1) error("spatial parameters length is wrong");
		if(npar != 2 && nug == 0) error("spatial parameters length is wrong");
		par1 = REAL(pars)[0];
		par2 = REAL(pars)[1];
		if(nug==1) nugpar = REAL(pars)[2];
		else nugpar = 0.0;
		par3 = 0.0;
	}

	PROTECT(V = allocMatrix(REALSXP, m, n));

	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			// This puts the nugget:sill ratio in off-diagonals instead of diagonal
			if(nug==1)
			{
				// Only works on a few models
				if(mod == 1 || mod == 2 || mod ==4 || mod == 5 || mod == 7)
				{
					var = (1 - nugpar) * spatvar(par1,par2,par3,REAL(D)[i*n+j],mod);
				}
				else
				{
					error("Not sure how to handle nugget here\n");
					//var = spatvar(par1,par2,par3,REAL(D)[i*n+j],mod)/(1-nugpar);
				}
			}
			else var = spatvar(par1,par2,par3,REAL(D)[i*n+j],mod);
			REAL(V)[i*n+j] = var;
			var = 0.0;

			// // This puts nugget:sill ratio only in diagonal components
			// var = spatvar(par1,par2,par3,REAL(D)[i*n+j],mod);
			// REAL(V)[i*n+j] = var;
			// var = 0.0;
		}
	}
	/*fix diagonal entries*/
	for(i=0;i<n;i++)
	{
		// This puts the nugget:sill ratio in off-diagonals instead of diagonal
		REAL(V)[i*n+i] = 1.0;

		// // This puts nugget:sill ratio only in diagonal components
		// if(nug==1) REAL(V)[i*n+i] = 1.0 / (1 - nugpar);
		// else REAL(V)[i*n+i] = 1.0;
	}

	UNPROTECT(2);
	return(V);
}

/* To calculate the spatial correlation, based upon geoR and RLS notes */;
double spatvar(double par1, double par2, double par3, double dij, int model)
{
	double vij;
	double phi;
	// Exponential
	if(model == 1)
	{
		if(dij/par1 <= 20)
		{
			vij = exp(-dij/par1);
		}
		else vij = 0.0;
	}
	// Gaussian
	if(model == 2)
	{
		vij = exp(-(dij*dij)/(par1*par1));
	}
	// Cubic
	if(model == 3)
	{
		if(dij >= par1) vij = 0.0;
		else vij = 7*R_pow(dij/par1,2) - 8.75*R_pow(dij/par1,3) - 0.75*R_pow(dij/par1,7);
	}
	// Spherical
	if(model == 4)
	{
		if(dij >= par1) vij = 0.0;
		else vij = 1 - 1.5*dij/par1 + 0.5*R_pow(dij/par1,3);
	}
	// Wave
	if (model == 5)
	{
		vij = par1 * sin(dij/par1) / dij;
	}
	// Linear (can't figure out what this should because not stationary
	if (model == 6)
	{
		error("This model is not working yet\n");
		vij = 0.0;
	}

	/* Two parameter models */;

	// Matern
	if(model == 7)
	{
		// RLS Course Notes version:
		if(par2 < 50)
		{
			phi = (2 * sqrt(par2)) / par1;
			vij = R_pow(dij*phi,par2)*bessel_k(dij*phi,par2,1)*(R_pow(2.0,-(par2-1))/gammafn(par2));
		}
		else vij = exp(-(dij*dij)/(par1*par1)); // Use Gaussian if par2 gets too big
		/*
		// geoR version
		if(dij > 600*par1) vij = 0;
		else
		{
			vij = (R_pow(2.0,-(par2-1))/gammafn(par2)) * R_pow(dij/par1,par2) 
				* bessel_k(dij/par1,par2,1);
		}
		*/;
	}
	// cauchy
	if(model == 8)
	{
		vij = R_pow(1+R_pow(dij/par1,2),par2);
	}
	// exponential power
	if(model == 9)
	{
		vij = exp(-R_pow(dij/par1,par2));
	}

	return(vij);
}
