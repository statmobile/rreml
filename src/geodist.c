#include "bjl.h"

/****************************************************************
 * This is to set a matrix of the geodesic distances
 * for two matrices listing out the lons and lats.  The first 
 * column for each input matrix should be the 
 * longitude (-180,180), and the second should be the latitude 
 * (-180,180).  
 ***************************************************************/

SEXP geodist(SEXP x, SEXP y)
{
	SEXP Dist, Dimsx, Dimsy;
	int i, j, n;

	PROTECT(Dimsx = getAttrib(x, R_DimSymbol));
	PROTECT(Dimsy = getAttrib(y, R_DimSymbol));

	if(INTEGER(Dimsx)[1] != 2 || INTEGER(Dimsy)[1] != 2)
	{
		error("X and Y need two columns for geodist.c\n");
	}
	n = INTEGER(Dimsx)[0];
	if(INTEGER(Dimsy)[0] != n) error("X and Y need to be same length for geodist.c\n");

	// Now set n x n matrix, and fill in the distances;
	PROTECT(Dist = allocMatrix(REALSXP, n, n));
	for(j=0;j<n;j++)
	{
		for(i=0;i<n;i++)
		{
			if(i==j) REAL(Dist)[i+n*j] = 0.0;
			else
			{
				REAL(Dist)[i+n*j] = dist(REAL(x)[i],REAL(x)[i+n],REAL(y)[j],REAL(y)[j+n]);
				// Rprintf("lon1 is %e and lat1 is %e\n",REAL(x)[i],REAL(x)[i+n]);
				// Rprintf("lon2 is %e and lat2 is %e\n",REAL(y)[i],REAL(y)[j+n]);
				// Rprintf("distance is %e\n",dist(REAL(x)[i],REAL(x)[i+n],REAL(y)[j],REAL(y)[j+n]));
			}
		}
	}
	UNPROTECT(3);
	return(Dist);
}

double dist(double lon1, double lat1, double lon2, double lat2)
{
	double x1, x2, y1, y2;
	SEXP d, T1;

	PROTECT(d = allocVector(REALSXP,1));
	PROTECT(T1 = allocVector(REALSXP, 1));
	// Great Circle Method
	// x1 = sin(lat1*M_PI/180)*sin(lat2*M_PI/180);
	// x2 = (cos(lat1*M_PI/180)*cos(lat2*M_PI/180)*cos(lon1*M_PI/180-lon2*M_PI/180));
	// d = 6378.7*acos(x1+x2);
	// Rprintf("x1 is %e\n",x1);
	// Rprintf("x2 is %e\n",x2);
	// Rprintf("x1 + x2 is %e\n",x1+x2);

	// From RLS Reference
	x1 = lon1*M_PI/180; y1 = lat1*M_PI/180;
	x2 = lon2*M_PI/180; y2 = lat2*M_PI/180;
//	Rprintf("x1 and x2 are %e and %e\n",x1, x2);
//	Rprintf("y1 and y2 are %e and %e\n",y1, y2);
	REAL(T1)[0] = (cos(y1)*cos(x1)-cos(y2)*cos(x2))*(cos(y1)*cos(x1)-cos(y2)*cos(x2))
		+(cos(y1)*sin(x1)-cos(y2)*sin(x2))*(cos(y1)*sin(x1)-cos(y2)*sin(x2))
		+(sin(y1)-sin(y2))*(sin(y1)-sin(y2));
//	Rprintf("T1 is %e\n",T1);
	REAL(d)[0] = 12732.4 * asin( sqrt(REAL(T1)[0]) / 2);
//	Rprintf("d is %e\n",d);

	UNPROTECT(2);
	return(REAL(d)[0]);
}
