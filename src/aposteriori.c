#include "bjl.h"

/****************************************************************
 * This function is proportional to the negative log of the a 
 * posteriori distribution 
 ***************************************************************/
   

SEXP aposteriori(SEXP pars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
		 SEXP vars, SEXP model, SEXP indexstarts, SEXP mu, 
		 SEXP Sigma, SEXP BGLS, SEXP nugget, SEXP invmethod)
{
	// General data for calculations
	SEXP beta, spatpars, kerns, logdets, priorcalc, priorX;
	// For block diagonal work
	SEXP Xi, coordsi, varsi, Vi, yi, gausslkd, kern, logdet;
	// To index the reprotected SEXP objects
	PROTECT_INDEX Xipi, coordsipi, varsipi, Vipi, yipi, 
		gausslkdpi, kernpi, logdetpi; 
	// Dims for block diagonal work
	SEXP r1, r2, XDIMS, p1, p2, coordp1, coordp2; 
	// proflkhd is w/out prior, result is with prior
	SEXP proflkhd, result; 
	// Prior proflkhd information
	SEXP priordet, priorkern;

	SEXP nis; //Get nis to find out largest one
	SEXP BGLSres; // To get results of BGLS

	int maxni, nug;  // Allocate maximum matrix
	int betap, spatp=1, N, mod, T, p;
	int ni, start, stop;
	int i, j;
	int monitor=0; // Set from 1-3 for levels of monitoring

	PROTECT(proflkhd = allocVector(REALSXP,1));
	PROTECT(result = allocVector(REALSXP,1));

	mod = INTEGER(model)[0];  // Get spatial model
	T = length(indexstarts); // Get vector of starting points
	N = length(y);
	nug = INTEGER(nugget)[0];

	PROTECT(r1 = allocVector(INTSXP,1)); 
	PROTECT(r2 = allocVector(INTSXP,1));
	PROTECT(XDIMS = getAttrib(X, R_DimSymbol));
	PROTECT(p1 = allocVector(INTSXP,1)); INTEGER(p1)[0] = 0;
	PROTECT(p2 = allocVector(INTSXP,1)); INTEGER(p2)[0] = INTEGER(XDIMS)[1]-1;
	PROTECT(coordp1 = allocVector(INTSXP,1)); INTEGER(coordp1)[0]=0;
	PROTECT(coordp2 = allocVector(INTSXP,1)); INTEGER(coordp2)[0]=1;

	p = INTEGER(XDIMS)[1];

	PROTECT(proflkhd = allocVector(REALSXP,1));
	PROTECT(logdets = allocVector(REALSXP,T));
	PROTECT(kerns = allocVector(REALSXP,T));

	/* Find maximum ni to allocate matrices */;
	PROTECT(nis = allocVector(INTSXP,T));
	if(T > 0)
	{
		for(i=0;i<T-1;i++) INTEGER(nis)[i] = INTEGER(indexstarts)[i+1]-INTEGER(indexstarts)[i];
		INTEGER(nis)[T-1] = N - INTEGER(indexstarts)[T-1]+1;
		R_isort(INTEGER(nis), T);
		maxni = INTEGER(nis)[T-1];
	}
	else maxni = N;

	/* Print pars to keep track of everything when monitoring*/;
	if(monitor > 0)
	{
		if(INTEGER(BGLS)==0)
		{
			for(i=0;i<(length(pars)-2);i++) Rprintf("%e  ",REAL(pars)[i]);
		}
		Rprintf("\n");
		if(mod>6 || nug==1)
		{
			if(mod>6 && nug==1)
			{
				Rprintf("%e\t%e\t%e\n",REAL(pars)[length(pars)-3],
					REAL(pars)[length(pars)-2],
					REAL(pars)[length(pars)-1]);
			}
			else
			{
				Rprintf("%e\t%e\n",REAL(pars)[length(pars)-2],REAL(pars)[length(pars)-1]);
			}
		}
		else Rprintf("%e\n",REAL(pars)[length(pars)-1]);
	}

	/* Set parameters for model */;
	if(mod > 6 || nug == 1) spatp=spatp+1;
	if(mod > 6 && nug == 1) spatp=spatp+1;
	PROTECT(spatpars = allocVector(REALSXP, spatp));
	if(mod <= 6) // For one parameter models
	{
		if(INTEGER(BGLS)[0]==0) // If we're not using BGLS
		{
			if(nug == 1) betap = length(pars) - 2; 
			else betap = length(pars) - 1; 
			PROTECT(beta = allocVector(REALSXP, betap));
			for(i=0;i<betap;i++) REAL(beta)[i] = REAL(pars)[i];
			REAL(spatpars)[0] = REAL(pars)[betap];
			// If we have nugget:sill ratio
			if(nug == 1)
			{
				REAL(spatpars)[1] = REAL(pars)[betap+1];
			}
		}
		else // Use BGLS (Will set beta=bhat in if statement later)
		{
			betap = INTEGER(XDIMS)[1];
			REAL(spatpars)[0] = REAL(pars)[0];
			// If we have nugget:sill ratio
			if(nug == 1 )
			{
				REAL(spatpars)[1] = REAL(pars)[1];
			}
		}
	}
	if(mod > 6 && mod <= 9) // For two parameter models
	{
		if(INTEGER(BGLS)[0]==0) // If we're not using BGLS
		{
			if(nug == 1) betap = length(pars) - 3; 
			else betap = length(pars) - 2; 
			PROTECT(beta = allocVector(REALSXP, betap));
			for(i=0;i<betap;i++) REAL(beta)[i] = REAL(pars)[i];
			REAL(spatpars)[0] = REAL(pars)[betap];
			REAL(spatpars)[1] = REAL(pars)[betap+1];
			// If we have nugget:sill ratio
			if(nug == 1)
			{
				REAL(spatpars)[2] = REAL(pars)[betap+2];
			}
		}
		else // Use BGLS (Will set beta=bhat in if statement later)
		{
			betap = INTEGER(XDIMS)[1];
			REAL(spatpars)[0] = REAL(pars)[0];
			REAL(spatpars)[1] = REAL(pars)[1];
			// If we have nugget:sill ratio
			if(nug == 1 )
			{
				REAL(spatpars)[2] = REAL(pars)[2];
			}
		}
	}

	if(monitor > 2) Rprintf("Before allocating matrices\n");

	/* Initialize dummy objects before being used in loop */;
	PROTECT_WITH_INDEX(yi = allocVector(REALSXP,maxni), &yipi);
	PROTECT_WITH_INDEX(Xi = allocMatrix(REALSXP,maxni,maxni), &Xipi);
	PROTECT_WITH_INDEX(coordsi = allocMatrix(REALSXP,maxni,2), &coordsipi);
	PROTECT_WITH_INDEX(varsi = allocVector(REALSXP,maxni), &varsipi);
	PROTECT_WITH_INDEX(Vi = allocMatrix(REALSXP,maxni,maxni), &Vipi);
	PROTECT_WITH_INDEX(gausslkd = allocVector(VECSXP, 2), &gausslkdpi);
	PROTECT_WITH_INDEX(kern = allocVector(REALSXP,1), &kernpi);
	PROTECT_WITH_INDEX(logdet = allocVector(REALSXP,1), &logdetpi);
	if(monitor > 2) Rprintf("After allocating matrices\n");

	/* If we're using BGLS, need to calculate xhat first looping over blocks */;
	if(INTEGER(BGLS)[0]==1) 
	{
		if(monitor > 2) Rprintf("Before call to bgls\n");
		PROTECT(BGLSres=bgls(spatpars,y,X,coords,Dscale,vars,mu,Sigma,model,
				     indexstarts,nugget,invmethod));
		PROTECT(beta=getListElement(BGLSres, "beta"));
		if(monitor > 2) Rprintf("After call to bgls\n");
	}

	/* Now loop through index of times */;
	for(i=0; i<T; i++)
	{
		/* If in the index for vector of observations */;
		if(T > 1)
		{
			start=INTEGER(indexstarts)[i]-1;
			stop=INTEGER(indexstarts)[i+1]-2;
			ni=stop-start+1;
		}
		/* If on the last set or if only one set */;
		if(i == T-1)
		{
			if(T==1) start=0;
			else start=INTEGER(indexstarts)[i]-1;
			stop=N-1;
			ni=stop-start+1;
		}

		if(monitor > 2) Rprintf("Tracking loop\n\ti is %d\n\tstart is %d\n\tstop is %d\n\tni is %d\n",i,start,stop,ni);

		// Set appropriate sizes for SEXP variables
		REPROTECT(yi = allocVector(REALSXP,ni), yipi);
		REPROTECT(Xi = allocMatrix(REALSXP,ni,ni), Xipi);
		REPROTECT(coordsi = allocMatrix(REALSXP,ni,2), coordsipi);
		REPROTECT(varsi = allocVector(REALSXP,ni), varsipi);
		REPROTECT(Vi = allocMatrix(REALSXP,ni,ni), Vipi);
		REPROTECT(gausslkd = allocVector(VECSXP, 2), gausslkdpi);
		REPROTECT(kern = allocVector(REALSXP,1), kernpi);
		REPROTECT(logdet = allocVector(REALSXP,1), logdetpi);

		INTEGER(r1)[0] = start;
		INTEGER(r2)[0] = stop;

		for(j=0;j<ni;j++) REAL(yi)[j] = REAL(y)[start+j];
		REPROTECT(Xi = subMatrix(X,r1,r2,p1,p2), Xipi);
		if(monitor > 2) Rprintf("Before coordsi\n");
		REPROTECT(coordsi = subMatrix(coords,r1,r2,coordp1,coordp2), coordsipi);
		for(j=0;j<ni;j++) REAL(varsi)[j] = REAL(vars)[start+j];
		REPROTECT(Vi = setV(spatpars,model,nugget,coordsi,varsi,Dscale),Vipi);

		/* Calculate sum of squares and determinant */;
		if(monitor > 2) Rprintf("Before kern and det calls\n");
		REPROTECT(gausslkd = mvnorm(yi,Xi,beta,Vi,invmethod), gausslkdpi);
		if(monitor > 2) Rprintf("kern\n");
		REPROTECT(kern = getListElement(gausslkd, "kern"), kernpi);
		if(monitor > 2) Rprintf("det\n");
		//REPROTECT(det = detX(Vi), detpi);
		REPROTECT(logdet = getListElement(gausslkd, "logdet"), logdetpi);
		if(monitor > 2) Rprintf("After kern and det calls\n");
		
		/* Assign these values to the vector across all time through T */;
		REAL(kerns)[i] = REAL(kern)[0];
		REAL(logdets)[i] = REAL(logdet)[0];
		
	}

	/* Sum up over each block */;
	if(monitor > 2) Rprintf("Summing up over blocks\n");
	REAL(proflkhd)[0] = 0.0;
	for(i=0;i<T;i++)
	{
		REAL(proflkhd)[0] += REAL(kerns)[i] + REAL(logdets)[i];
	}

	// Add prior distribution 
	if(monitor > 2) Rprintf("Calculating prior\n");
	PROTECT(priorX = allocMatrix(REALSXP,betap,betap)); // This should be identity
	for(j=0;j<betap;j++)for(i=0;i<betap;i++)REAL(priorX)[i+j*betap]=0.0;
	for(i=0;i<betap;i++) REAL(priorX)[i+i*betap] = 1.0;
	PROTECT(priorcalc = mvnorm(beta,priorX,mu,Sigma,invmethod));
	PROTECT(priorkern = getListElement(priorcalc, "kern"));
	PROTECT(priordet = getListElement(priorcalc, "logdet"));
	
	if(monitor > 2) Rprintf("Calculating result\n");
	REAL(result)[0] = REAL(proflkhd)[0] +
		REAL(priordet)[0] + 
		REAL(priorkern)[0];

	// Rprintf("Results from prior are det = %e and kern = %e\n",REAL(priordet)[0],REAL(priorkern)[0]);
	// Rprintf("Result is %e\n",REAL(result)[0]);

	if(INTEGER(BGLS)[0]==0) UNPROTECT(27);
	else UNPROTECT(28);
	return(result);
}


