#include "bjl.h"

/****************************************************************
 * This function is the derivative of aposteriori with respect 
 * to the spatial parameters
 ***************************************************************/


SEXP daposteriori(SEXP pars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
		  SEXP vars, SEXP model, SEXP indexstarts, SEXP mu, 
		  SEXP Sigma, SEXP BGLS, SEXP nugget, SEXP invmethod)
{
	// General data for calculations
	SEXP beta, spatpars;
	// For block diagonal work
	SEXP Xi, coordsi, calc1, calc2, calc3, varsi, Vi, yi, 
		dVi1, dVi2, dVi3;
	// To index the reprotected SEXP objects
	PROTECT_INDEX Xipi, coordsipi, calc1pi, calc2pi, calc3pi, varsipi, 
		Vipi, yipi, dVi1pi, dVi2pi, dVi3pi;
	// For step by step calculations
      	SEXP ss1a, ss1b, ss1c, ss2a, ss2b, ss2c, trVdVa, trVdVb, trVdVc;
	// To index the reprotected SEXP objects
	PROTECT_INDEX ss1api, ss1bpi, ss1cpi, ss2api, ss2bpi, ss2cpi, 
		trVdVapi, trVdVbpi, trVdVcpi;
	// Dims for block diagonal work
	SEXP r1, r2, XDIMS, p1, p2, coordp1, coordp2; 
	// derivative of aposteriori
	SEXP deriv, derivsa, derivsb, derivsc;
	// derivative of prior information
	SEXP Sigmacalc, Sigmainv, dpriorcalc1, dpriorcalc2, dpriorcalc3, dprior1, dprior2, dprior3;
	// Derivative of Bayesian GLS
	SEXP dbeta, dbeta1, dbeta2, dbeta3;
	// Derivative wrt spatial parameter
	SEXP thetai1, thetai2, thetai3;

	SEXP nis; //Get nis to find out largest one
	SEXP BGLSres; // To get results of BGLS

	int derivlen=1;
	int maxni, nug;  // Allocate maximum matrix
	int betap, spatp=1, N, mod, T, p;
	int ni, start, stop;
	int i, j;
	int monitor=0; // Set from 1-3 for levels of monitoring

	if(INTEGER(BGLS)[0]==0) error("No point calculating derivative w.r.t. beta\n");

	mod = INTEGER(model)[0];  // Get spatial model
	T = length(indexstarts); // Get vector of starting points
	N = length(y);
	nug = INTEGER(nugget)[0];

	PROTECT(r1 = allocVector(INTSXP,1)); 
	PROTECT(r2 = allocVector(INTSXP,1));
	PROTECT(XDIMS = getAttrib(X, R_DimSymbol));
	PROTECT(p1 = allocVector(INTSXP,1)); INTEGER(p1)[0] = 0;
	PROTECT(p2 = allocVector(INTSXP,1)); INTEGER(p2)[0] = INTEGER(XDIMS)[1]-1;
	PROTECT(coordp1 = allocVector(INTSXP,1)); INTEGER(coordp1)[0]=0;
	PROTECT(coordp2 = allocVector(INTSXP,1)); INTEGER(coordp2)[0]=1;

	p = INTEGER(XDIMS)[1];

	// Set length of derivative
	if(mod > 6 || nug == 1) derivlen=derivlen+1;
	if(mod > 6 && nug == 1) derivlen=derivlen+1;
	PROTECT(deriv=allocVector(REALSXP, derivlen));
	if(monitor > 2) Rprintf("derivlen is %d\n",derivlen);

	PROTECT(derivsa = allocVector(REALSXP,T));
	PROTECT(derivsb = allocVector(REALSXP,T));
	PROTECT(derivsc = allocVector(REALSXP,T));

	// Set index of derivatives
	PROTECT(thetai1 = allocVector(INTSXP,1));
	INTEGER(thetai1)[0] = 1;
	PROTECT(thetai2 = allocVector(INTSXP,1));
	INTEGER(thetai2)[0] = 2;
	PROTECT(thetai3 = allocVector(INTSXP,1));
	INTEGER(thetai3)[0] = 3;

	/* Find maximum ni to allocate matrices */;
	PROTECT(nis = allocVector(INTSXP,T));
	if(T > 0)
	{
		for(i=0;i<T-1;i++) INTEGER(nis)[i] = INTEGER(indexstarts)[i+1]-INTEGER(indexstarts)[i];
		INTEGER(nis)[T-1] = N - INTEGER(indexstarts)[T-1]+1;
		R_isort(INTEGER(nis), T);
		maxni = INTEGER(nis)[T-1];
	}
	else maxni = N;

	/* Print pars to keep track of everything when monitoring*/;
	if(monitor > 0)
	{
		if(INTEGER(BGLS)==0)
		{
			for(i=0;i<(length(pars)-2);i++) Rprintf("%e  ",REAL(pars)[i]);
		}
		Rprintf("\n");
		if(mod>6 || nug ==1)
		{
			if(mod>6 && nug==1)
			{
				Rprintf("%e\t%e\t%e\n",REAL(pars)[length(pars)-3],
					REAL(pars)[length(pars)-2],
					REAL(pars)[length(pars)-1]);
			}
			else
			{
				Rprintf("%e\t%e\n",REAL(pars)[length(pars)-2],REAL(pars)[length(pars)-1]);
			}
		}
		else Rprintf("%e\n",REAL(pars)[length(pars)-1]);
	}

	/* Set parameters for model */;
	if(mod > 6 || nug == 1) spatp=spatp+1;
	if(mod > 6 && nug == 1) spatp=spatp+1;
	PROTECT(spatpars = allocVector(REALSXP, spatp));
	if(mod <= 6) // For one parameter models
	{
		// Use BGLS (Will set beta=bhat in if statement later)
		betap = INTEGER(XDIMS)[1];
		REAL(spatpars)[0] = REAL(pars)[0];
		// If we have nugget:sill ratio
		if(nug == 1 )
		{
			REAL(spatpars)[1] = REAL(pars)[1];
		}
	}
	if(mod > 6 && mod <= 9) // For two parameter models
	{
		// Use BGLS (Will set beta=bhat in if statement later)
		betap = INTEGER(XDIMS)[1];
		REAL(spatpars)[0] = REAL(pars)[0];
		REAL(spatpars)[1] = REAL(pars)[1];
		// If we have nugget:sill ratio
		if(nug == 1)
		{
			REAL(spatpars)[2] = REAL(pars)[2];
		}
	}

	if(monitor > 2) Rprintf("Before allocating matrices\n");

	/* Initialize dummy objects before being used in loop */;
	PROTECT_WITH_INDEX(yi = allocVector(REALSXP,maxni), &yipi);
	PROTECT_WITH_INDEX(Xi = allocMatrix(REALSXP,maxni,maxni), &Xipi);
	PROTECT_WITH_INDEX(coordsi = allocMatrix(REALSXP,maxni,2), &coordsipi);
	PROTECT_WITH_INDEX(varsi = allocVector(REALSXP,maxni), &varsipi);
	PROTECT_WITH_INDEX(Vi = allocMatrix(REALSXP,maxni,maxni), &Vipi);
	PROTECT_WITH_INDEX(dVi1 = allocMatrix(REALSXP,maxni,maxni), &dVi1pi);
	PROTECT_WITH_INDEX(dVi2 = allocMatrix(REALSXP,maxni,maxni), &dVi2pi);
	PROTECT_WITH_INDEX(dVi3 = allocMatrix(REALSXP,maxni,maxni), &dVi3pi);
	PROTECT_WITH_INDEX(calc1 = allocVector(VECSXP,3), &calc1pi);
	PROTECT_WITH_INDEX(calc2 = allocVector(VECSXP,3), &calc2pi);
	PROTECT_WITH_INDEX(calc3 = allocVector(VECSXP,3), &calc3pi);
	PROTECT_WITH_INDEX(ss1a = allocVector(REALSXP,1), &ss1api);
	PROTECT_WITH_INDEX(ss1b = allocVector(REALSXP,1), &ss1bpi);
	PROTECT_WITH_INDEX(ss1c = allocVector(REALSXP,1), &ss1cpi);
	PROTECT_WITH_INDEX(ss2a = allocVector(REALSXP,1), &ss2api);
	PROTECT_WITH_INDEX(ss2b = allocVector(REALSXP,1), &ss2bpi);
	PROTECT_WITH_INDEX(ss2c = allocVector(REALSXP,1), &ss2cpi);
	PROTECT_WITH_INDEX(trVdVa = allocVector(REALSXP,1), &trVdVapi);
	PROTECT_WITH_INDEX(trVdVb = allocVector(REALSXP,1), &trVdVbpi);
	PROTECT_WITH_INDEX(trVdVc = allocVector(REALSXP,1), &trVdVcpi);
	if(monitor > 2) Rprintf("After allocating matrices\n");

	/* If we're using BGLS, need to calculate xhat first looping over blocks */;
	if(INTEGER(BGLS)[0]==1) 
	{
		if(monitor > 2) Rprintf("Before call to bgls\n");
		PROTECT(BGLSres=bgls(spatpars,y,X,coords,Dscale,vars,mu,Sigma,model,
				     indexstarts,nugget,invmethod));
		PROTECT(beta=getListElement(BGLSres, "beta"));
		if(monitor > 2) Rprintf("After call to bgls\n");
		PROTECT(dbeta=dbgls(spatpars,y,X,coords,Dscale,vars,mu,Sigma,model,
				    indexstarts,nugget,invmethod));
		if(monitor > 2) Rprintf("After call to dbgls\n");
	}

	/* Now set each one of the vectors of derivatives*/
	PROTECT(dbeta1 = allocVector(REALSXP,betap));
	PROTECT(dbeta2 = allocVector(REALSXP,betap));
	PROTECT(dbeta3 = allocVector(REALSXP,betap));
	for(j=0;j<betap;j++)
	{
		REAL(dbeta1)[j]=REAL(dbeta)[j];
		if(mod > 6 || nug == 1) REAL(dbeta2)[j]=REAL(dbeta)[betap+j];
		if(mod > 6 && nug == 1) REAL(dbeta3)[j]=REAL(dbeta)[2*betap+j];
	}

	/* Now loop through index of times */;
	for(i=0; i<T; i++)
	{
		/* If in the index for vector of observations */;
		if(T > 1)
		{
			start=INTEGER(indexstarts)[i]-1;
			stop=INTEGER(indexstarts)[i+1]-2;
			ni=stop-start+1;
		}
		/* If on the last set or if only one set */;
		if(i == T-1)
		{
			if(T==1) start=0;
			else start=INTEGER(indexstarts)[i]-1;
			stop=N-1;
			ni=stop-start+1;
		}

		if(monitor > 2) Rprintf("Tracking loop\n\ti is %d\n\tstart is %d\n\tstop is %d\n\tni is %d\n",i,start,stop,ni);

		// Set appropriate sizes for SEXP variables
		REPROTECT(yi = allocVector(REALSXP,ni), yipi);
		REPROTECT(Xi = allocMatrix(REALSXP,ni,ni), Xipi);
		REPROTECT(coordsi = allocMatrix(REALSXP,ni,2), coordsipi);
		REPROTECT(varsi = allocVector(REALSXP,ni), varsipi);
		REPROTECT(Vi = allocMatrix(REALSXP,ni,ni), Vipi);
		REPROTECT(dVi1 = allocMatrix(REALSXP,ni,ni), dVi1pi);
		REPROTECT(dVi2 = allocMatrix(REALSXP,ni,ni), dVi2pi);
		REPROTECT(dVi3 = allocMatrix(REALSXP,ni,ni), dVi3pi);
		REPROTECT(calc1 = allocVector(VECSXP, 3), calc1pi);
		REPROTECT(calc2 = allocVector(VECSXP, 3), calc2pi);
		REPROTECT(calc3 = allocVector(VECSXP, 3), calc3pi);
		REPROTECT(ss1a = allocVector(REALSXP,1), ss1api);
		REPROTECT(ss1b = allocVector(REALSXP,1), ss1bpi);
		REPROTECT(ss1c = allocVector(REALSXP,1), ss1cpi);
		REPROTECT(ss2a = allocVector(REALSXP,1), ss2api);
		REPROTECT(ss2b = allocVector(REALSXP,1), ss2bpi);
		REPROTECT(ss2c = allocVector(REALSXP,1), ss2cpi);
		REPROTECT(trVdVa = allocVector(REALSXP,1), trVdVapi);
		REPROTECT(trVdVb = allocVector(REALSXP,1), trVdVbpi);
		REPROTECT(trVdVc = allocVector(REALSXP,1), trVdVcpi);

		INTEGER(r1)[0] = start;
		INTEGER(r2)[0] = stop;

		for(j=0;j<ni;j++) REAL(yi)[j] = REAL(y)[start+j];
		REPROTECT(Xi = subMatrix(X,r1,r2,p1,p2), Xipi);
		REPROTECT(coordsi = subMatrix(coords,r1,r2,coordp1,coordp2), coordsipi);
		for(j=0;j<ni;j++) REAL(varsi)[j]=REAL(vars)[start+j];
		REPROTECT(Vi = setV(spatpars, model, nugget, coordsi, varsi, Dscale), Vipi);
		// Calculate derivatives based upon models
		REPROTECT(dVi1 = setdV(thetai1,spatpars,model,nugget,coordsi,varsi,Dscale), dVi1pi);
		if(mod > 6 || nug == 1) REPROTECT(dVi2 = setdV(thetai2,spatpars,model,nugget,coordsi,varsi,Dscale), dVi2pi);
		if(mod > 6 && nug == 1) REPROTECT(dVi3 = setdV(thetai3,spatpars,model,nugget,coordsi,varsi,Dscale), dVi3pi);

		/* Calculate sum of squares and determinant */;
		REPROTECT(calc1 = dapostcalc(Xi,Vi,dVi1,beta,dbeta1,yi,invmethod), calc1pi);
		REPROTECT(trVdVa = getListElement(calc1,"trvdv"),trVdVapi);
		REPROTECT(ss1a = getListElement(calc1,"dkern"),ss1api);
		REPROTECT(ss2a = getListElement(calc1,"ssdbeta"),ss2api);
		// If second derivative calculate it
		if(mod > 6 || nug == 1)
		{
			REPROTECT(calc2 = dapostcalc(Xi,Vi,dVi2,beta,dbeta2,yi,invmethod),calc2pi);
			REPROTECT(trVdVb = getListElement(calc2,"trvdv"),trVdVbpi);
			REPROTECT(ss1b = getListElement(calc2,"dkern"),ss1bpi);
			REPROTECT(ss2b = getListElement(calc2,"ssdbeta"),ss2bpi);
		}
		// If third derivative calculate it
		if(mod > 6 && nug == 1)
		{
			REPROTECT(calc3 = dapostcalc(Xi,Vi,dVi3,beta,dbeta3,yi,invmethod),calc3pi);
			REPROTECT(trVdVc = getListElement(calc3,"trvdv"),trVdVcpi);
			REPROTECT(ss1c = getListElement(calc3,"dkern"),ss1cpi);
			REPROTECT(ss2c = getListElement(calc3,"ssdbeta"),ss2cpi);
		}

		/* Assign these values to the vector across all time through T */;
		REAL(derivsa)[i] = REAL(trVdVa)[0]-REAL(ss1a)[0]-2*REAL(ss2a)[0];
		if(mod > 6 || nug == 1) REAL(derivsb)[i]=REAL(trVdVb)[0]-REAL(ss1b)[0]-2*REAL(ss2b)[0];
		if(mod > 6 && nug == 1) REAL(derivsc)[i]=REAL(trVdVc)[0]-REAL(ss1c)[0]-2*REAL(ss2c)[0];
	}

	/* Sum up over each block */;
	REAL(deriv)[0] = 0.0;
	for(i=0;i<T;i++)
	{
		REAL(deriv)[0] += REAL(derivsa)[i];
	}
	if(mod > 6 || nug == 1)
	{
		REAL(deriv)[1] = 0.0;
		for(i=0;i<T;i++)
		{
			REAL(deriv)[1] += REAL(derivsb)[i];
		}
	}
	if(mod > 6 && nug == 1)
	{
		REAL(deriv)[2] = 0.0;
		for(i=0;i<T;i++)
		{
			REAL(deriv)[2] += REAL(derivsc)[i];
		}
	}

	// Now calculate the prior part of the derivative
	PROTECT(Sigmacalc = matrixcalc(Sigma,invmethod));
	PROTECT(Sigmainv = getListElement(Sigmacalc,"Vinv"));
	PROTECT(dpriorcalc1 = XminusY(beta,mu));
	PROTECT(dpriorcalc2 = tX(dpriorcalc1));
	PROTECT(dpriorcalc3 = XtimesY(dpriorcalc2,Sigmainv));
	PROTECT(dprior1 = XtimesY(dpriorcalc3,dbeta1));
	if(mod > 6 || nug == 1) PROTECT(dprior2 = XtimesY(dpriorcalc3,dbeta2));
	if(mod > 6 && nug == 1) PROTECT(dprior3 = XtimesY(dpriorcalc3,dbeta3));
	// And add it to the other derivatives
	REAL(deriv)[0] = REAL(deriv)[0] + 2 * REAL(dprior1)[0];
	if(mod > 6 || nug == 1) REAL(deriv)[1] = REAL(deriv)[1] + 2 * REAL(dprior2)[0];
	if(mod > 6 && nug == 1) REAL(deriv)[2] = REAL(deriv)[2] + 2 * REAL(dprior3)[0];
	
	// Different unprotects due to dprior1 - dprior3
	if(spatp == 3) UNPROTECT(50);
	if(spatp == 2) UNPROTECT(49);
	if(spatp == 1) UNPROTECT(48);
	return(deriv);
}

