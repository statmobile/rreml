/* 
 * These are functions copied from 
 * - R-2.5.0/src/modules/lapack/Lapack.c
 * They are used in this package, and because they are not "public"
 * the source code needs to be copied into this package
 * Note that I replaced "mod" with "bjl" in the function names
 */

#include "bjl.h"

/* ------------------------------------------------------------ */

SEXP bjlLa_chol(SEXP A)
{
    if (isMatrix(A)) {
	SEXP ans = PROTECT((TYPEOF(A) == REALSXP)?duplicate(A):
			   coerceVector(A, REALSXP));
	SEXP adims = getAttrib(A, R_DimSymbol);
	int m = INTEGER(adims)[0];
	int n = INTEGER(adims)[1];
	int i, j;

	if (m != n) error("'a' must be a square matrix");
	if (m <= 0) error("'a' must have dims > 0");
	for (j = 0; j < n; j++) {	/* zero the lower triangle */
	    for (i = j+1; i < n; i++) {
		REAL(ans)[i + j * n] = 0.;
	    }
	}

	F77_CALL(dpotrf)("Upper", &m, REAL(ans), &m, &i);
	if (i != 0) {
	    if (i > 0)
		error("the leading minor of order %d is not positive definite",
		      i);
	    error("argument %d of Lapack routine %s had invalid value",
		  -i, "dpotrf");
	}
	unprotect(1);
	return ans;
    }
    else error("'a' must be a numeric matrix");
    return R_NilValue; /* -Wall */
}

/* ------------------------------------------------------------ */

SEXP bjlLa_chol2inv(SEXP A, SEXP size)
{
    int sz = asInteger(size);
    if (sz == NA_INTEGER || sz < 1)
	error("'size' argument must be a positive integer");
    if (isMatrix(A)) {
	SEXP Amat = PROTECT(coerceVector(A, REALSXP));
	SEXP ans;
	SEXP adims = getAttrib(A, R_DimSymbol);
	int m = INTEGER(adims)[0];
	int n = INTEGER(adims)[1];
	int i, j;

	if (sz > n) error("'size' cannot exceed ncol(x) = %d", n);
	if (sz > m) error("'size' cannot exceed nrow(x) = %d", m);
	ans = PROTECT(allocMatrix(REALSXP, sz, sz));
	for (j = 0; j < sz; j++) {
	    for (i = 0; i <= j; i++)
		REAL(ans)[i + j * sz] = REAL(Amat)[i + j * m];
	}
	F77_CALL(dpotri)("Upper", &sz, REAL(ans), &sz, &i);
	if (i != 0) {
	    if (i > 0)
		error("element (%d, %d) is zero, so the inverse cannot be computed", i, i);
	    error("argument %d of Lapack routine %s had invalid value",
		  -i, "dpotri");
	}
	for (j = 0; j < sz; j++) {
	    for (i = j+1; i < sz; i++)
		REAL(ans)[i + j * sz] = REAL(ans)[j + i * sz];
	}
	unprotect(2);
	return ans;
    }
    else error("'a' must be a numeric matrix");
    return R_NilValue; /* -Wall */
}

/* ------------------------------------------------------------ */

SEXP bjldet_ge_real(SEXP Ain, SEXP logarithm)
{
    int i, n, *Adims, info, *jpvt, sign, useLog;
    double modulus = 0.0; /* -Wall */
    SEXP val, nm, A;

    if (!(isMatrix(Ain) && isReal(Ain)))
	error("'a' must be a numeric matrix");
    useLog = asLogical(logarithm);
    if (useLog == NA_LOGICAL) error("argument 'logarithm' must be logical");
    PROTECT(A = duplicate(Ain));
    Adims = INTEGER(coerceVector(getAttrib(A, R_DimSymbol), INTSXP));
    n = Adims[0];
    if (Adims[1] != n)
	error("'a' must be a square matrix");
    jpvt = (int *) R_alloc(n, sizeof(int));
    F77_CALL(dgetrf)(&n, &n, REAL(A), &n, jpvt, &info);
    sign = 1;
    if (info < 0)
	error("error code %d from Lapack routine '%s'", info, "dgetrf");
    else if (info > 0) { /* Singular matrix:  U[i,i] (i := info) is 0 */
	/*warning("Lapack dgetrf(): singular matrix: U[%d,%d]=0", info,info);*/
	modulus = (useLog ? R_NegInf : 0.);
    }
    else {
	for (i = 0; i < n; i++) if (jpvt[i] != (i + 1))
	    sign = -sign;
	if (useLog) {
	    modulus = 0.0;
	    for (i = 0; i < n; i++) {
		double dii = REAL(A)[i*(n + 1)]; /* ith diagonal element */
		modulus += log(dii < 0 ? -dii : dii);
		if (dii < 0) sign = -sign;
	    }
	} else {
	    modulus = 1.0;
	    for (i = 0; i < n; i++)
		modulus *= REAL(A)[i*(n + 1)];
	    if (modulus < 0) {
		modulus = -modulus;
		sign = -sign;
	    }
	}
    }
    val = PROTECT(allocVector(VECSXP, 2));
    nm = PROTECT(allocVector(STRSXP, 2));
    SET_STRING_ELT(nm, 0, mkChar("modulus"));
    SET_STRING_ELT(nm, 1, mkChar("sign"));
    setAttrib(val, R_NamesSymbol, nm);
    SET_VECTOR_ELT(val, 0, ScalarReal(modulus));
    setAttrib(VECTOR_ELT(val, 0), install("logarithm"), ScalarLogical(useLog));
    SET_VECTOR_ELT(val, 1, ScalarInteger(sign));
    setAttrib(val, R_ClassSymbol, ScalarString(mkChar("det")));
    UNPROTECT(3);
    return val;
}

/* ------------------------------------------------------------ */

SEXP bjlLa_dgesv(SEXP A, SEXP Bin, SEXP tolin)
{
    int n, p, info, *ipiv, *Adims, *Bdims;
    double *avals, anorm, rcond, tol = asReal(tolin), *work;
    SEXP B;

    if (!(isMatrix(A) && isReal(A)))
	error("'a' must be a numeric matrix");
    if (!(isMatrix(Bin) && isReal(Bin)))
	error("'b' must be a numeric matrix");
    PROTECT(B = duplicate(Bin));
    Adims = INTEGER(coerceVector(getAttrib(A, R_DimSymbol), INTSXP));
    Bdims = INTEGER(coerceVector(getAttrib(B, R_DimSymbol), INTSXP));
    n = Adims[0];
    if(n == 0) error("'a' is 0-diml");
    p = Bdims[1];
    if(p == 0) error("no right-hand side in 'b'");
    if(Adims[1] != n)
	error("'a' (%d x %d) must be square", n, Adims[1]);
    if(Bdims[0] != n)
	error("'b' (%d x %d) must be compatible with 'a' (%d x %d)",
	      Bdims[0], p, n, n);
    ipiv = (int *) R_alloc(n, sizeof(int));

    avals = (double *) R_alloc(n * n, sizeof(double));
				/* work on a copy of A */
    Memcpy(avals, REAL(A), (size_t) (n * n));
    F77_CALL(dgesv)(&n, &p, avals, &n, ipiv, REAL(B), &n, &info);
    if (info < 0)
	error("argument %d of Lapack routine %s had invalid value",
	      -info, "dgesv");
    if (info > 0)
	error("Lapack routine dgesv: system is exactly singular");
    anorm = F77_CALL(dlange)("1", &n, &n, REAL(A), &n, (double*) NULL);
    work = (double *) R_alloc(4*n, sizeof(double));
    F77_CALL(dgecon)("1", &n, avals, &n, &anorm, &rcond, work, ipiv, &info);
    if (rcond < tol)
	error("system is computationally singular: reciprocal condition number = %g",
	      rcond);
    UNPROTECT(1);
    return B;
}

/* ------------------------------------------------------------ */

SEXP bjlLa_svd(SEXP jobu, SEXP jobv, SEXP x, SEXP s, SEXP u, SEXP v,
		      SEXP method)
{
    int *xdims, n, p, lwork, info = 0;
    double *work, *xvals, tmp;
    SEXP val, nm;
    const char *meth;

    if (!(isString(jobu) && isString(jobv)))
	error("'jobu' and 'jobv' must be character strings");
    if (!isString(method))
	error("'method' must be a character string");
    meth = CHAR(STRING_ELT(method, 0));
    xdims = INTEGER(coerceVector(getAttrib(x, R_DimSymbol), INTSXP));
    n = xdims[0]; p = xdims[1];
    xvals = (double *) R_alloc(n * p, sizeof(double));
    /* work on a copy of x */
    Memcpy(xvals, REAL(x), (size_t) (n * p));

    {
	int ldu = INTEGER(getAttrib(u, R_DimSymbol))[0],
	    ldvt = INTEGER(getAttrib(v, R_DimSymbol))[0];
	int *iwork= (int *) R_alloc(8*(n<p ? n : p), sizeof(int));

	/* ask for optimal size of work array */
	lwork = -1;
	F77_CALL(dgesdd)(CHAR(STRING_ELT(jobu, 0)),
			 &n, &p, xvals, &n, REAL(s),
			 REAL(u), &ldu,
			 REAL(v), &ldvt,
			 &tmp, &lwork, iwork, &info);
	if (info != 0)
	    error("error code %d from Lapack routine '%s'", info, "dgesdd");
	lwork = (int) tmp;
	work = (double *) R_alloc(lwork, sizeof(double));
	F77_CALL(dgesdd)(CHAR(STRING_ELT(jobu, 0)),
			 &n, &p, xvals, &n, REAL(s),
			 REAL(u), &ldu,
			 REAL(v), &ldvt,
			 work, &lwork, iwork, &info);
	if (info != 0)
	    error("error code %d from Lapack routine '%s'", info, "dgesdd");
    }

    val = PROTECT(allocVector(VECSXP, 3));
    nm = PROTECT(allocVector(STRSXP, 3));
    SET_STRING_ELT(nm, 0, mkChar("d"));
    SET_STRING_ELT(nm, 1, mkChar("u"));
    SET_STRING_ELT(nm, 2, mkChar("vt"));
    setAttrib(val, R_NamesSymbol, nm);
    SET_VECTOR_ELT(val, 0, s);
    SET_VECTOR_ELT(val, 1, u);
    SET_VECTOR_ELT(val, 2, v);
    UNPROTECT(2);
    return val;
}

/* ------------------------------------------------------------ */

SEXP bjlLa_rs(SEXP xin, SEXP only_values)
{
    int *xdims, n, lwork, info = 0, ov;
    char jobv[1], uplo[1], range[1];
    SEXP values, ret, nm, x, z = R_NilValue;
    double *work, *rx, *rvalues, tmp, *rz = NULL;
    int liwork, *iwork, itmp, m;
    double vl = 0.0, vu = 0.0, abstol = 0.0;
    /* valgrind seems to think vu should be set, but it is documented
       not to be used if range='a' */
    int il, iu, *isuppz;

    PROTECT(x = duplicate(xin));
    rx = REAL(x);
    uplo[0] = 'L';
    xdims = INTEGER(coerceVector(getAttrib(x, R_DimSymbol), INTSXP));
    n = xdims[0];
    if (n != xdims[1])
	error("'x' must be a square numeric matrix");
    ov = asLogical(only_values);
    if (ov == NA_LOGICAL) error("invalid 'only.values'");
    if (ov) jobv[0] = 'N'; else jobv[0] = 'V';

    PROTECT(values = allocVector(REALSXP, n));
    rvalues = REAL(values);

    range[0] = 'A';
    if (!ov) {
	PROTECT(z = allocMatrix(REALSXP, n, n));
	rz = REAL(z);
    }
    isuppz = (int *) R_alloc(2*n, sizeof(int));
    /* ask for optimal size of work arrays */
    lwork = -1; liwork = -1;
    F77_CALL(dsyevr)(jobv, range, uplo, &n, rx, &n,
		     &vl, &vu, &il, &iu, &abstol, &m, rvalues,
		     rz, &n, isuppz,
		     &tmp, &lwork, &itmp, &liwork, &info);
    if (info != 0)
	error("error code %d from Lapack routine '%s'", info, "dsyevr");
    lwork = (int) tmp;
    liwork = itmp;

    work = (double *) R_alloc(lwork, sizeof(double));
    iwork = (int *) R_alloc(liwork, sizeof(int));
    F77_CALL(dsyevr)(jobv, range, uplo, &n, rx, &n,
		     &vl, &vu, &il, &iu, &abstol, &m, rvalues,
		     rz, &n, isuppz,
		     work, &lwork, iwork, &liwork, &info);
    if (info != 0)
	error("error code %d from Lapack routine '%s'", info, "dsyevr");

    if (!ov) {
	ret = PROTECT(allocVector(VECSXP, 2));
	nm = PROTECT(allocVector(STRSXP, 2));
	SET_STRING_ELT(nm, 1, mkChar("vectors"));
	SET_VECTOR_ELT(ret, 1, z);
	UNPROTECT_PTR(z);
    } else {
	ret = PROTECT(allocVector(VECSXP, 1));
	nm = PROTECT(allocVector(STRSXP, 1));
    }
    SET_STRING_ELT(nm, 0, mkChar("values"));
    setAttrib(ret, R_NamesSymbol, nm);
    SET_VECTOR_ELT(ret, 0, values);
    UNPROTECT(4);
    return ret;
}


