/****************************************************************
 * These are various linear algebra SEXP wrappers for BLAS and
 * LAPACK routines supplied in R.
 *
 * XtimesY and XTtimesY are similar to functions found in R 
 * source code src/main/array.c, where we use BLAS functions
 *
 ***************************************************************/
   
#include "bjl.h"

/* Function to convert a SEXP vector to a SEXP matrix */;
SEXP vec2mat(SEXP X)
{
	int i, nx;
	SEXP Xmat;

	if(isMatrix(X))
	{
		return(X);
	}
	if(isVector(X))
	{
		nx = length(X);
		PROTECT(Xmat = allocMatrix(REALSXP,nx,1));
		for(i=0;i<nx;i++) REAL(Xmat)[i]=REAL(X)[i];
		UNPROTECT(1);
		return(Xmat);
	}
	else error("vec2mat only works on a matrix or vector");
}

/* get the list element named str, or return NULL */
SEXP getListElement(SEXP list, char *str)
{
        SEXP elmt = R_NilValue, names = getAttrib(list, R_NamesSymbol);
        int i;
        for (i = 0; i < length(list); i++)
        if(strcmp(CHAR(STRING_ELT(names, i)), str) == 0) {
                elmt = VECTOR_ELT(list, i);
                break;
        }
      return elmt;
}

/* (nx x px) X times (ny x py) Y = (nx x py) Z */
SEXP XtimesY(SEXP X, SEXP Y)
{
	int nx, px, ny, py;
	double one = 1.0, zero = 0.0;
	char *transa = "N", *transb = "N";
	SEXP dimsX, dimsY, Xmat, Ymat, Z;

	// If a vector, coerce into matrix
	if(!isMatrix(X)) PROTECT(Xmat = vec2mat(X));
	else PROTECT(Xmat = X);
	if(!isMatrix(Y)) PROTECT(Ymat = vec2mat(Y));
	else PROTECT(Ymat = Y);

	// If it isn't REALSXP, coerce that way
	if(TYPEOF(Xmat) != REALSXP) error("X matrix is not REALSXP in XtimesY");
	if(TYPEOF(Ymat) != REALSXP) error("Y matrix is not REALSXP in XtimesY");

	// Now get dimensions of the matrices
	PROTECT(dimsX = getAttrib(Xmat, R_DimSymbol));
	PROTECT(dimsY = getAttrib(Ymat, R_DimSymbol));
	nx = INTEGER(dimsX)[0]; px = INTEGER(dimsX)[1];
	ny = INTEGER(dimsY)[0]; py = INTEGER(dimsY)[1];

	if(px != ny) error("Dimensions in XtimesY do not match");

	PROTECT(Z = allocMatrix(REALSXP, nx, py));
	F77_CALL(dgemm)(transa, transb, &nx, &py, &px, &one,
			REAL(Xmat), &nx, REAL(Ymat), &ny, &zero, REAL(Z), &nx);

	UNPROTECT(5);
	return(Z);
}

/* (nx x px) X times (ny x py) Y = (nx x py) Z */
SEXP XTtimesY(SEXP X, SEXP Y)
{
	int nx, px, ny, py;
	double one = 1.0, zero = 0.0;
	char *transa = "N", *transb = "N";
	SEXP dimsX, dimsY, Xmat, XTmat, Ymat, Z;

	error("There seem to be errors with the function XTtimesY");

	// If a vector, coerce into matrix
	if(!isMatrix(X)) PROTECT(Xmat = vec2mat(X));
	else PROTECT(Xmat = X);
	if(!isMatrix(Y)) PROTECT(Ymat = vec2mat(Y));
	else PROTECT(Ymat = Y);

	// Take transpose of matrix
	PROTECT(XTmat = tX(Xmat));

	// If it isn't REALSXP, coerce that way
	if(TYPEOF(Xmat) != REALSXP) error("X matrix is not REALSXP in XTtimesY");
	if(TYPEOF(Ymat) != REALSXP) error("Y matrix is not REALSXP in XTtimesY");

	// Now get dimensions of the matrices
	PROTECT(dimsX = getAttrib(XTmat, R_DimSymbol));
	PROTECT(dimsY = getAttrib(Ymat, R_DimSymbol));
	nx = INTEGER(dimsX)[0]; px = INTEGER(dimsX)[1];
	ny = INTEGER(dimsY)[0]; py = INTEGER(dimsY)[1];

	if(px != ny) error("Dimensions in XTtimesY do not match");

	PROTECT(Z = allocMatrix(REALSXP, nx, py));
	F77_CALL(dgemm)(transa, transb, &nx, &py, &px, &one,
			REAL(Xmat), &nx, REAL(Ymat), &ny, &zero, REAL(Z), &nx);

	UNPROTECT(6);
	return(Z);
}

/* (nx x px) X - (ny x py) Y = (nx x py) Z */
SEXP XminusY(SEXP X, SEXP Y)
{
	int i, j, nx, px, ny, py;
	SEXP DIMSX, DIMSY, Z, Xmat, Ymat;

	// If a vector, coerce into matrix
	if(!isMatrix(X)) PROTECT(Xmat = vec2mat(X));
	else PROTECT(Xmat = X);
	if(!isMatrix(Y)) PROTECT(Ymat = vec2mat(Y));
	else PROTECT(Ymat = Y);

	// If it isn't REALSXP, coerce that way
	if(TYPEOF(Xmat) != REALSXP) error("X matrix is not REALSXP in XminusY");
	if(TYPEOF(Ymat) != REALSXP) error("Y matrix is not REALSXP in XminusY");

	PROTECT(DIMSX = getAttrib(Xmat, R_DimSymbol));
	PROTECT(DIMSY = getAttrib(Ymat, R_DimSymbol));
	nx = INTEGER(DIMSX)[0]; px = INTEGER(DIMSX)[1];
	ny = INTEGER(DIMSY)[0]; py = INTEGER(DIMSY)[1];

	PROTECT(Z = allocMatrix(REALSXP,nx,px));

	if(px != py) error("Dimensions don't match for XminusY");
	if(nx != ny) error("Dimensions don't match for XminusY");

	for(j = 0; j < px; j++)
	{
		for(i = 0; i < nx; i++)
		{
			REAL(Z)[i+nx*j] = REAL(Xmat)[i+nx*j]-REAL(Ymat)[i+nx*j];
		}
	}
	UNPROTECT(5);
	return(Z);
}

/* (nx x px) X + (ny x py) Y = (nx x py) Z */
SEXP XplusY(SEXP X, SEXP Y)
{
	int i, j, nx, px, ny, py;
	SEXP DIMSX, DIMSY, Z, Xmat, Ymat;

	// If a vector, coerce into matrix
	if(!isMatrix(X)) PROTECT(Xmat = vec2mat(X));
	else PROTECT(Xmat = X);
	if(!isMatrix(Y)) PROTECT(Ymat = vec2mat(Y));
	else PROTECT(Ymat = Y);

	// If it isn't REALSXP, coerce that way
	if(TYPEOF(Xmat) != REALSXP) error("X matrix is not REALSXP in XminusY");
	if(TYPEOF(Ymat) != REALSXP) error("Y matrix is not REALSXP in XminusY");

	PROTECT(DIMSX = getAttrib(Xmat, R_DimSymbol));
	PROTECT(DIMSY = getAttrib(Ymat, R_DimSymbol));
	nx = INTEGER(DIMSX)[0]; px = INTEGER(DIMSX)[1];
	ny = INTEGER(DIMSY)[0]; py = INTEGER(DIMSY)[1];

	PROTECT(Z = allocMatrix(REALSXP,nx,px));

	if(px != py) error("Dimensions don't match for XminusY");
	if(nx != ny) error("Dimensions don't match for XminusY");

	for(j = 0; j < px; j++)
	{
		for(i = 0; i < nx; i++)
		{
			REAL(Z)[i+nx*j] = REAL(Xmat)[i+nx*j]+REAL(Ymat)[i+nx*j];
		}
	}
	UNPROTECT(5);
	return(Z);
}

/* Calculate the trace of X */
SEXP tr(SEXP X)
{
	int i, m, n;
	SEXP trace, DIMSX;

	if(!isMatrix(X)) error("Can only calculate trace of matrix");
	PROTECT(DIMSX = getAttrib(X, R_DimSymbol));
	m = INTEGER(DIMSX)[0];
	n = INTEGER(DIMSX)[1];
	if(m!=n) error("Can only take trace of square matrix");

	PROTECT(trace = allocVector(REALSXP,1));
	REAL(trace)[0] = 0.0;

	for(i=0;i<n;i++) REAL(trace)[0] += REAL(X)[i+n*i];

	UNPROTECT(2);
	return(trace);
}

/* transpose of (nx x px) X */
SEXP tX(SEXP X)
{
	int i, j, nx, px;
	SEXP Z, Xmat, DIMSX;

	// If a vector, coerce into matrix
	if(!isMatrix(X)) PROTECT(Xmat = vec2mat(X));
	else PROTECT(Xmat = X);

	PROTECT(DIMSX = getAttrib(Xmat, R_DimSymbol));
	nx = INTEGER(DIMSX)[0]; px = INTEGER(DIMSX)[1];

	PROTECT(Z = allocMatrix(REALSXP,px,nx));

	for(j = 0; j < nx; j++)
	{
		for(i = 0; i < px; i++)
		{
			REAL(Z)[j*px+i] = REAL(Xmat)[i*nx+j];
		}
	}
	UNPROTECT(3);
	return(Z);
}

/* Calculate determinant of a matrix (Good reference for dealing with lists) */
SEXP detX(SEXP V)
{
        SEXP det, RESULT, LOGARITHM;
        int sgn;

        PROTECT(det = allocVector(REALSXP,1));
        PROTECT(LOGARITHM = allocVector(LGLSXP,1));
        LOGICAL(LOGARITHM)[0] = TRUE;

        PROTECT(RESULT = bjldet_ge_real(V,LOGARITHM));

        sgn = INTEGER(VECTOR_ELT(RESULT, 1))[0];
        REAL(det)[0] = sgn * exp(REAL(getListElement(RESULT, "modulus"))[0]); //Using function for list element

//        R_PV(RESULT);  // To see class structure in R

        UNPROTECT(3);
        return(det); 
}

/* Using cholesky decomposition, calculate V^{-1} */
SEXP cholinvV(SEXP V)
{
        SEXP Vchol, Vinv, DIMS, size;
        int M, N;

        PROTECT(DIMS = getAttrib(V,R_DimSymbol));
	M = INTEGER(DIMS)[0];
        N = INTEGER(DIMS)[1];

	// Don't need to check dims, as La_chol2inv does this already
        if (M != N) error("V needs to be a square matrix in cholinvV");

	PROTECT(size = allocVector(INTSXP,1));

	INTEGER(size)[0] = M;

	PROTECT(Vchol = bjlLa_chol(V));  // First Cholesky decomposition
	PROTECT(Vinv = bjlLa_chol2inv(Vchol,size)); // Next get inverse

        UNPROTECT(4);
        // return(Vchol);
        return(Vinv);
}

/* Caclulate SVD of square matrix A = UDV^T gives D, U and V^T */
SEXP svdsqV(SEXP A)
{
	SEXP res, DIMS, M, N, ds, U, VT;
	int i, j;
        SEXP jobu, jobv, method;

	PROTECT(jobu = allocVector(STRSXP, 1));
	SET_STRING_ELT(jobu, 0, mkChar("S"));
	PROTECT(jobv = allocVector(STRSXP, 1));
	SET_STRING_ELT(jobv, 0, mkChar(""));
	PROTECT(method = allocVector(STRSXP, 1));
	SET_STRING_ELT(method, 0, mkChar("dgsedd"));

	PROTECT(DIMS = getAttrib(A,R_DimSymbol));
	PROTECT(M = allocVector(INTSXP,1));
	PROTECT(N = allocVector(INTSXP,1));

	INTEGER(M)[0] = INTEGER(DIMS)[0];
	INTEGER(N)[0] = INTEGER(DIMS)[1];

	// Make sure it's a square matrix
	if(INTEGER(M)[0] != INTEGER(N)[0]) error("V needs to be squre matrix in svdsqV");

	// Setup matrices (not for complex)
	PROTECT(U = allocMatrix(REALSXP,INTEGER(M)[0],INTEGER(M)[0]));
	PROTECT(VT = allocMatrix(REALSXP,INTEGER(M)[0],INTEGER(M)[0]));
	PROTECT(ds = allocVector(REALSXP,INTEGER(M)[0]));
	for(j=0;j<INTEGER(M)[0];j++)
	{
		REAL(ds)[j] = 0.0;
		for(i=0;i<INTEGER(M)[0];i++)
		{
			REAL(U)[i+j*INTEGER(M)[0]] = 0.0;
			REAL(VT)[i+j*INTEGER(M)[0]] = 0.0;
		}
	}

	PROTECT(res = bjlLa_svd(jobu,jobv,A,ds,U,VT,method));
	UNPROTECT(10);
	return(res);
}

/* Using SVD calculate A^{-1} */
SEXP svdinvV(SEXP A)
{
	SEXP DIMS, SVD, D, U, VT, Dinv;
	SEXP pt1, V, UT, Ainv;
	int i, j;

	PROTECT(DIMS = getAttrib(A,R_DimSymbol));

	// Calculate SVD of square A
	PROTECT(SVD = svdsqV(A));

	PROTECT(D = getListElement(SVD,"d"));
	PROTECT(U = getListElement(SVD,"u"));
	PROTECT(VT = getListElement(SVD,"vt"));

	// Set diagonal matrix for Dinv of zeros, and reciprocal of singular values
	PROTECT(Dinv = allocMatrix(REALSXP,INTEGER(DIMS)[0],INTEGER(DIMS)[0]));
	for(j=0;j<INTEGER(DIMS)[0];j++)
	{
		for(i=0;i<INTEGER(DIMS)[0];i++)
		{
			REAL(Dinv)[i+j*INTEGER(DIMS)[0]] = 0.0;
		}
	}
	for(i=0;i<INTEGER(DIMS)[0];i++)
	{
		REAL(Dinv)[i+i*INTEGER(DIMS)[0]] = 1/REAL(D)[i];
	}

	// Now calculate the inverse
	PROTECT(V = tX(VT));
	PROTECT(pt1 = XtimesY(V,Dinv));
	PROTECT(UT = tX(U));
	PROTECT(Ainv = XtimesY(pt1,UT));

	UNPROTECT(10);
	return(Ainv);
}

/* Caclulate spectral decomposition inverse of symmetric square matrix A = UDU^T */
SEXP spectinvV(SEXP A)
{
	SEXP Ainv, eigen, only_values, DIMS, U, D;
	SEXP Dinv, UT, pt1;
	int i, j;

	PROTECT(DIMS = getAttrib(A,R_DimSymbol));
	PROTECT(only_values = allocVector(LGLSXP,1));
	LOGICAL(only_values)[0] = 0; // Set only_values to FALSE

	if(INTEGER(DIMS)[0] != INTEGER(DIMS)[1])
		error("Not square matrix for spectinvV");

	PROTECT(eigen = bjlLa_rs(A, only_values));
	PROTECT(D = getListElement(eigen,"values"));
	PROTECT(U = getListElement(eigen,"vectors"));

	// Set inverse of Diagonal of eigenvalues
	PROTECT(Dinv = allocMatrix(REALSXP,INTEGER(DIMS)[0],INTEGER(DIMS)[0]));
	for(j=0;j<INTEGER(DIMS)[0];j++)
	{
		for(i=0;i<INTEGER(DIMS)[0];i++)
		{
			REAL(Dinv)[i+j*INTEGER(DIMS)[0]] = 0.0;
		}
	}
	for(i=0;i<INTEGER(DIMS)[0];i++)
	{
		REAL(Dinv)[i+i*INTEGER(DIMS)[0]] = 1/REAL(D)[i];
	}

	PROTECT(UT = tX(U));
	PROTECT(pt1 = XtimesY(U,Dinv));
	PROTECT(Ainv = XtimesY(pt1,UT));

	UNPROTECT(9);
	return(Ainv);
}


/* Special program to calculate the inverse and log absolute value determinant all in one call
   method=0 uses Cholesky, method=1 uses spectral decomposition,  method=2 uses SVD */
SEXP matrixcalc(SEXP V, SEXP method)
{
	SEXP Vcalc, Vinv, det, result, names, DIMS, size;
	SEXP pt1, D, Dinv, svdV, svdVT, UT, U, only_values; // calculations
        int i, j, M, N, protects=0;
	double prod=0.0;

        PROTECT(DIMS = getAttrib(V,R_DimSymbol));
	M = INTEGER(DIMS)[0];
        N = INTEGER(DIMS)[1];

	// Don't need to check dims, as La_chol2inv does this already
        if (M != N) error("V needs to be a square matrix in matrixcalc");

	PROTECT(size = allocVector(INTSXP,1));
	PROTECT(det = allocVector(REALSXP,1));

	INTEGER(size)[0] = M;

	// Setup list for both inverse and determinant
	PROTECT(result = allocVector(VECSXP, 2));
	PROTECT(names = allocVector(STRSXP, 2));
	setAttrib(result, R_NamesSymbol, names);
	// Name the list elements
	SET_STRING_ELT(names,0,mkChar("Vinv"));
	SET_STRING_ELT(names,1,mkChar("logdet"));

	protects = 5; // PROTECTS up to this point
	// For Cholesky Decomposition
	if(INTEGER(method)[0]==0)
	{
		PROTECT(Vcalc = bjlLa_chol(V));  // First Cholesky decomposition
		PROTECT(Vinv = bjlLa_chol2inv(Vcalc,size)); // Next get inverse
		SET_VECTOR_ELT(result,0,Vinv);
		
		// The determinant is product of diagonal of Vchol squared
		// This is on the log scale of absolute value
		for(i=0;i<M;i++) prod = prod + log(REAL(Vcalc)[i+M*i]);
		REAL(det)[0] = 2 * prod;
		SET_VECTOR_ELT(result,1,det);

		protects = protects + 2;
	}
	// For spectral decomposition matrix
	if(INTEGER(method)[0]==1)
	{
		PROTECT(only_values = allocVector(LGLSXP,1));
		LOGICAL(only_values)[0] = 0; // Set only_values to FALSE

		PROTECT(Vcalc = bjlLa_rs(V,only_values));
		PROTECT(U = getListElement(Vcalc,"vectors"));
		PROTECT(D = getListElement(Vcalc,"values"));
		// Set Dinv
		PROTECT(Dinv = allocMatrix(REALSXP,M,M));
		for(j=0;j<M;j++) for(i=0;i<M;i++) REAL(Dinv)[i+j*M] = 0.0;
		for(i=0;i<M;i++)
		{
			REAL(Dinv)[i+M*i] = 1/REAL(D)[i];
			// Product of eigenvalues is determinant, this is log scale
			prod = prod + log(REAL(D)[i]);
		}

		PROTECT(UT = tX(U));
		PROTECT(pt1 = XtimesY(U,Dinv));
		PROTECT(Vinv = XtimesY(pt1,UT));
		REAL(det)[0] = prod;

		SET_VECTOR_ELT(result,0,Vinv);
		SET_VECTOR_ELT(result,1,det);

		protects = protects + 8;
	}
	// For SVD inverse
	if(INTEGER(method)[0]==2)
	{
		PROTECT(Vcalc = svdsqV(V));
		PROTECT(D = getListElement(Vcalc,"d"));
		PROTECT(U = getListElement(Vcalc,"u"));
		PROTECT(svdVT = getListElement(Vcalc,"vt"));

		PROTECT(svdV = tX(svdVT));
		PROTECT(UT = tX(U));
		// Set Dinv
		PROTECT(Dinv = allocMatrix(REALSXP,M,M));
		for(j=0;j<M;j++) for(i=0;i<M;i++) REAL(Dinv)[i+j*M] = 0.0;
		for(i=0;i<M;i++)
		{
			REAL(Dinv)[i+M*i] = 1/REAL(D)[i];
			// Eigenvalues are singular values (for positive def matrix)
			// Product of eigenvalues is determinant, this is log scale
			prod = prod + log(REAL(D)[i]);
		}

		PROTECT(pt1 = XtimesY(svdV,Dinv));
		PROTECT(Vinv = XtimesY(pt1,UT));
		REAL(det)[0] = prod;

		SET_VECTOR_ELT(result,0,Vinv);
		SET_VECTOR_ELT(result,1,det);

		protects = protects +9;
	}


	if(INTEGER(method)[0]!=0 && INTEGER(method)[0]!=1)
		if(INTEGER(method)[0]!=2) error("What method to use for matrixcalc?");

	UNPROTECT(protects);
	return(result);
}
		
/* Generic inverse of a matrix using LAPACK routine */
SEXP Xinv(SEXP X)
{
	SEXP TOL, Xinv, I, DIMS;
	int i, j, n;

	PROTECT(DIMS = getAttrib(X, R_DimSymbol));
	n = INTEGER(DIMS)[0];

	PROTECT(Xinv = allocMatrix(REALSXP,n,n));
	PROTECT(I = allocMatrix(REALSXP,n,n));
	PROTECT(TOL = allocVector(REALSXP,1));

	/*Set identity*/;
	for(j=0;j<n;j++) for (i=0;i<n;i++) REAL(I)[j*n+i]=0.0;
	for(j=0;j<n;j++) REAL(I)[j*n+j]=1.0;

	REAL(TOL)[0] = 1e-07; //Standard tolerance in R

	Xinv = bjlLa_dgesv(X,I,TOL);

	UNPROTECT(4);
	return(Xinv);
}

/* C code to get X[r1:r2,c1:c2] */
SEXP subMatrix(SEXP X, SEXP r1, SEXP r2, SEXP c1, SEXP c2) 
{
	SEXP Xsubset, DIMS;
	int i, j, n, p, nr, nc, *col2=INTEGER(c2), *row1=INTEGER(r1);
	int *col1=INTEGER(c1), *row2=INTEGER(r2);

	PROTECT(DIMS = getAttrib(X, R_DimSymbol));

	n = INTEGER(DIMS)[0]; p = INTEGER(DIMS)[1];
	nr = *row2 - *row1 + 1;
	nc = *col2 - *col1 + 1;

	if(nc > p || nr > n) error("Index out of bounds of Matrix");

	PROTECT(Xsubset = allocMatrix(REALSXP,nr,nc));

	for(j=0;j<nc;j++)
	{
		for(i=0;i<nr;i++)
		{
			REAL(Xsubset)[j * nr + i] = REAL(X)[(*col1 * n + *row1) + i + n * j];
			//REAL(Xsubset)[j * nr + i] = j * nr + i;
		}
	}

	UNPROTECT(2);
	return(Xsubset);
}
				
/* Code to sum across list of vectors */
SEXP sumVectorList(SEXP veclist)
{
	SEXP sum;
	int i, t, n, sumn;

	// First make sure that veclist is a list
	if(!isVectorList(veclist)) error("sumVecList is only for Vector Lists");

	n = length(veclist);

	// Copy first element in list, then zero out vector
	sumn = length(VECTOR_ELT(veclist,0));
	PROTECT(sum = allocVector(REALSXP,sumn));
	for(i=0;i<sumn;i++) REAL(sum)[i]=0.0;

	for(i=0;i<sumn;i++)
	{
		for(t=0;t<n;t++)
		{
			REAL(sum)[i] += REAL(VECTOR_ELT(veclist,t))[i];
		}
	}

	UNPROTECT(1);
	return(sum);
}

/* Code to sum across list of matrices */
SEXP sumMatrixList(SEXP matlist)
{
	SEXP sum, DIMS;
	int i, j, t, n;
	int matn, matp; // Dims of n x p matrix

	// First make sure that matlist is a list
	if(!isVectorList(matlist)) error("sumMatrixList is only for Matrix Lists");

	n = length(matlist);

	// Copy first element in list, then zero out matrix
	PROTECT(DIMS = getAttrib(VECTOR_ELT(matlist,0), R_DimSymbol));
	matn = INTEGER(DIMS)[0]; matp = INTEGER(DIMS)[1];
	PROTECT(sum = allocMatrix(REALSXP,matn,matp));

	//Rprintf("Length of matrix list is %d dim of matrix is %d x %d\n",n,matn,matp);

	for(j=0;j<matp;j++)
	{
		for(i=0;i<matn;i++)
		{
			REAL(sum)[i+j*matn] = 0.0;
			for(t=0;t<n;t++)
			{
				REAL(sum)[i+j*matn] += REAL(VECTOR_ELT(matlist,t))[i+j*matn];
			}
		}
	}
	
	UNPROTECT(2);
	return(sum);
}
