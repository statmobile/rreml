#include "bjl.h"

/****************************************************************
 * This function calculates the derivative of the Bayesian 
 * Generalized Least Squares estimate w.r.t. the spatial 
 * parameters
 ***************************************************************/

/* This is just a wrapper to drbgls with lambda=1 */
SEXP dbgls(SEXP spatpars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
	   SEXP vars, SEXP mu, SEXP Sigma, SEXP model, SEXP indexstarts, 
	   SEXP nugget, SEXP invmethod)
{
	SEXP lambda, typeII, result;

	PROTECT(lambda=allocVector(REALSXP,1));
	PROTECT(typeII=allocVector(LGLSXP,1));
	REAL(lambda)[0]=1.0;
	INTEGER(typeII)[0]=0;

	PROTECT(result = drbgls(spatpars, lambda, y, X, coords, Dscale, vars, 
				mu, Sigma, model, indexstarts, nugget, invmethod, typeII));

	UNPROTECT(3);
	return(result);
}


SEXP drbgls(SEXP spatpars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
	    SEXP vars, SEXP mu, SEXP Sigma, SEXP model, SEXP indexstarts, 
	    SEXP nugget, SEXP invmethod, SEXP typeII)
{
	// Variables for calculating estimate in block diagonal form
	SEXP yi, Xi, coordsi, varsi, Vi, dVi1, dVi2, dVi3; 
	PROTECT_INDEX yipi, Xipi, coordsipi, varsipi, Vipi, 
		dVi1pi, dVi2pi, dVi3pi;

	// Block diagonal using Bayesian Generalized Least Squares
	SEXP calc1, calc2, calc3, BLS1i, BLS2i, dBLS1ia, dBLS1ib, dBLS1ic, dBLS2ia, dBLS2ib, dBLS2ic, 
		BLS1s, BLS2s, dBLS1sa, dBLS1sb, dBLS1sc, dBLS2sa, dBLS2sb, dBLS2sc, 
		bhat1, bhat2, dbhat, dbhat1a, dbhat1b, dbhat1c, dbhat2a, dbhat2b, dbhat2c;
	PROTECT_INDEX calc1pi, calc2pi, calc3pi, BLS1ipi, BLS2ipi, dBLS1iapi, dBLS1ibpi, dBLS1icpi, dBLS2iapi,
		dBLS2ibpi, dBLS2icpi;

	// Matrices used in calculating the final derivative of betahat
	SEXP BGLSmat1, BGLSmat1inv, BGLSmat2, BGLSmat3, Sigmacalc, Sigmainv, cSigmainv;
	SEXP calc1a, calc2a, calc3a, calc4a, calc5a,
		calc1b, calc2b, calc3b, calc4b, calc5b,
		calc1c, calc2c, calc3c, calc4c, calc5c;

	// Variables for calculating derivative w.r.t. lambda
	SEXP dlam, dlam1, dlam2, dlam3, dlam4;

	// Dims for block diagonal work
	SEXP r1, r2, XDIMS, p1, p2, coordp1, coordp2; 
	// Derivative wrt spatial parameter
	SEXP thetai1, thetai2, thetai3;

	SEXP nis, rpars, rlambda; //Get nis to find out largest one in block diagonal
	int maxni, nug;
	int derivlen=1;

	int N, T, betap, mod;
	int ni, start, stop;
	int i, j;
	int monitor=0; // Set from 1-3 for levels of monitoring

	/* If using typeII method then get lambda from first element of pars */;
	PROTECT(rlambda = allocVector(REALSXP,1));
	if(INTEGER(typeII)[0]==1)
	{
		PROTECT(rpars = allocVector(REALSXP,length(spatpars)-1));
		REAL(rlambda)[0]=REAL(spatpars)[0];
		for(i=0;i<length(rpars);i++) REAL(rpars)[i]=REAL(spatpars)[i+1];
	}
	else
	{
		PROTECT(rpars = allocVector(REALSXP,length(spatpars)));
		for(i=0;i<length(rpars);i++) REAL(rpars)[i]=REAL(spatpars)[i];
		REAL(rlambda)[0] = REAL(lambda)[0];
	}

	mod = INTEGER(model)[0];  // Get spatial model
	T = length(indexstarts); // Get vector of starting points
	N = length(y);
	nug = INTEGER(nugget)[0];

	if(monitor > 2) Rprintf("T is %d and N is %d\n",T,N);

	PROTECT(r1 = allocVector(INTSXP,1)); 
	PROTECT(r2 = allocVector(INTSXP,1));
	PROTECT(XDIMS = getAttrib(X, R_DimSymbol));
	PROTECT(p1 = allocVector(INTSXP,1)); INTEGER(p1)[0] = 0;
	PROTECT(p2 = allocVector(INTSXP,1)); INTEGER(p2)[0] = INTEGER(XDIMS)[1]-1;
	PROTECT(coordp1 = allocVector(INTSXP,1)); INTEGER(coordp1)[0]=0;
	PROTECT(coordp2 = allocVector(INTSXP,1)); INTEGER(coordp2)[0]=1;
	betap = INTEGER(XDIMS)[1];

	// Set length of derivative
	if(mod > 6 || nug == 1) derivlen=derivlen+1;
	if(mod > 6 && nug == 1) derivlen=derivlen+1;
	if(INTEGER(typeII)[0]==1) derivlen=derivlen+1;
	PROTECT(dbhat=allocMatrix(REALSXP, betap, derivlen));

	// Set index of derivatives
	PROTECT(thetai1 = allocVector(INTSXP,1));
	INTEGER(thetai1)[0] = 1;
	PROTECT(thetai2 = allocVector(INTSXP,1));
	INTEGER(thetai2)[0] = 2;
	PROTECT(thetai3 = allocVector(INTSXP,1));
	INTEGER(thetai3)[0] = 3;

	/* Find maximum ni to allocate matrices */;
	PROTECT(nis = allocVector(INTSXP,T));
	if(T > 0)
	{
		for(i=0;i<T-1;i++) INTEGER(nis)[i] = INTEGER(indexstarts)[i+1]-INTEGER(indexstarts)[i];
		INTEGER(nis)[T-1] = N - INTEGER(indexstarts)[T-1]+1;
		R_isort(INTEGER(nis), T);
		maxni = INTEGER(nis)[T-1];
	}
	else maxni = N;

	PROTECT(BLS1s = allocVector(VECSXP, T));
	PROTECT(dBLS1sa = allocVector(VECSXP, T));
	PROTECT(dBLS1sb = allocVector(VECSXP, T));
	PROTECT(dBLS1sc = allocVector(VECSXP, T));
	PROTECT(BLS2s = allocVector(VECSXP, T));
	PROTECT(dBLS2sa = allocVector(VECSXP, T));
	PROTECT(dBLS2sb = allocVector(VECSXP, T));
	PROTECT(dBLS2sc = allocVector(VECSXP, T));

	/* Initialize dummy objects before being used in loop */;
	PROTECT_WITH_INDEX(yi = allocVector(REALSXP,maxni), &yipi);
	PROTECT_WITH_INDEX(Xi = allocMatrix(REALSXP,maxni,maxni), &Xipi);
	PROTECT_WITH_INDEX(coordsi = allocMatrix(REALSXP,maxni,2), &coordsipi);
	PROTECT_WITH_INDEX(varsi = allocVector(REALSXP,maxni), &varsipi);
	PROTECT_WITH_INDEX(Vi = allocMatrix(REALSXP,maxni,maxni), &Vipi);
	PROTECT_WITH_INDEX(dVi1 = allocMatrix(REALSXP,maxni,maxni), &dVi1pi);
	PROTECT_WITH_INDEX(dVi2 = allocMatrix(REALSXP,maxni,maxni), &dVi2pi);
	PROTECT_WITH_INDEX(dVi3 = allocMatrix(REALSXP,maxni,maxni), &dVi3pi);
	PROTECT_WITH_INDEX(calc1 = allocVector(VECSXP, 4), &calc1pi);
	PROTECT_WITH_INDEX(calc2 = allocVector(VECSXP, 4), &calc2pi);
	PROTECT_WITH_INDEX(calc3 = allocVector(VECSXP, 4), &calc3pi);
	PROTECT_WITH_INDEX(BLS1i = allocMatrix(REALSXP,betap,betap), &BLS1ipi);
	PROTECT_WITH_INDEX(dBLS1ia = allocMatrix(REALSXP,betap,betap), &dBLS1iapi);
	PROTECT_WITH_INDEX(dBLS1ib = allocMatrix(REALSXP,betap,betap), &dBLS1ibpi);
	PROTECT_WITH_INDEX(dBLS1ic = allocMatrix(REALSXP,betap,betap), &dBLS1icpi);
	PROTECT_WITH_INDEX(BLS2i = allocVector(REALSXP,betap), &BLS2ipi);
	PROTECT_WITH_INDEX(dBLS2ia = allocVector(REALSXP,betap), &dBLS2iapi);
	PROTECT_WITH_INDEX(dBLS2ib = allocVector(REALSXP,betap), &dBLS2ibpi);
	PROTECT_WITH_INDEX(dBLS2ic = allocVector(REALSXP,betap), &dBLS2icpi);

	/* Now loop through index of times */;
	for(i=0; i<T; i++)
	{
		/* If in the index for vector of observations */;
		if(T > 1)
		{
			start=INTEGER(indexstarts)[i]-1;
			stop=INTEGER(indexstarts)[i+1]-2;
			ni=stop-start+1;
		}
		/* If on the last set or if only one set */;
		if(i == T-1)
		{
			if(T==1) start=0;
			else start=INTEGER(indexstarts)[i]-1;
			stop=N-1;
			ni=stop-start+1;
		}
		
		if(monitor > 2) Rprintf("Tracking loop\n\ti is %d\n\tstart is %d\n\tstop is %d\n\tni is %d\n",i,start,stop,ni);
		
		REPROTECT(yi = allocVector(REALSXP,ni), yipi);
		REPROTECT(Xi = allocMatrix(REALSXP,ni,ni), Xipi);
		REPROTECT(coordsi = allocMatrix(REALSXP,ni,2), coordsipi);
		REPROTECT(varsi = allocVector(REALSXP,ni), varsipi);
		REPROTECT(Vi = allocMatrix(REALSXP,ni,ni), Vipi);
		REPROTECT(dVi1 = allocMatrix(REALSXP,ni,ni), dVi1pi);
		REPROTECT(dVi2 = allocMatrix(REALSXP,ni,ni), dVi2pi);
		REPROTECT(dVi3 = allocMatrix(REALSXP,ni,ni), dVi3pi);
		REPROTECT(calc1 = allocVector(VECSXP, 4), calc1pi);
		REPROTECT(calc2 = allocVector(VECSXP, 4), calc2pi);
		REPROTECT(calc3 = allocVector(VECSXP, 4), calc3pi);
		REPROTECT(BLS1i = allocMatrix(REALSXP,betap,betap), BLS1ipi);
		REPROTECT(dBLS1ia = allocMatrix(REALSXP,betap,betap), dBLS1iapi);
		REPROTECT(dBLS1ib = allocMatrix(REALSXP,betap,betap), dBLS1ibpi);
		REPROTECT(dBLS1ic = allocMatrix(REALSXP,betap,betap), dBLS1icpi);
		REPROTECT(BLS2i = allocVector(REALSXP,betap), BLS2ipi);
		REPROTECT(dBLS2ia = allocVector(REALSXP,betap), dBLS2iapi);
		REPROTECT(dBLS2ib = allocVector(REALSXP,betap), dBLS2ibpi);
		REPROTECT(dBLS2ic = allocVector(REALSXP,betap), dBLS2icpi);
		if(monitor > 2) Rprintf("After reprotecting block data\n");
		
		INTEGER(r1)[0] = start;
		INTEGER(r2)[0] = stop;
		
		for(j=0;j<ni;j++) REAL(yi)[j] = REAL(y)[start+j];
		REPROTECT(Xi = subMatrix(X,r1,r2,p1,p2), Xipi);
		// This was for old code that took whole Distance matrix as input
		// REPROTECT(Di = subMatrix(D,r1,r2,r1,r2), Dipi);
		if(monitor > 2) Rprintf("Before coordsi\n");
		REPROTECT(coordsi = subMatrix(coords,r1,r2,coordp1,coordp2), coordsipi);
		if(monitor > 2) Rprintf("After coordsi\n");

		for(j=0;j<ni;j++) REAL(varsi)[j] = REAL(vars)[start+j];
		REPROTECT(Vi = setV(rpars, model, nugget, coordsi, varsi, Dscale), Vipi);
		// Calculate derivatives based upon models
		REPROTECT(dVi1 = setdV(thetai1,rpars,model,nugget,coordsi,varsi,Dscale), dVi1pi);
		if(mod > 6 || nug == 1) REPROTECT(dVi2 = setdV(thetai2,rpars,model,nugget,coordsi,varsi,Dscale), dVi2pi);
		if(mod > 6 && nug == 1) REPROTECT(dVi3 = setdV(thetai3,rpars,model,nugget,coordsi,varsi,Dscale), dVi3pi);
		
		REPROTECT(calc1 = dbglscalc(Xi,Vi,dVi1,yi,invmethod), calc1pi);
		REPROTECT(BLS1i = getListElement(calc1, "xtvx"), BLS1ipi);
		REPROTECT(dBLS1ia = getListElement(calc1, "xtvdvvx"), dBLS1iapi);
		REPROTECT(BLS2i = getListElement(calc1, "xtvy"), BLS2ipi);
		REPROTECT(dBLS2ia = getListElement(calc1, "xtvdvvy"), dBLS2iapi);
		// If there's a second derivative do the calculations
		if(mod > 6 || nug == 1)
		{
			REPROTECT(calc2 = dbglscalc(Xi,Vi,dVi2,yi,invmethod), calc2pi);
			REPROTECT(dBLS1ib = getListElement(calc2, "xtvdvvx"), dBLS1ibpi);
			REPROTECT(dBLS2ib = getListElement(calc2, "xtvdvvy"), dBLS2ibpi);
		}
		// IF there's a third derivative do the calculations
		if(mod > 6 && nug == 1)
		{
			REPROTECT(calc3 = dbglscalc(Xi,Vi,dVi3,yi,invmethod), calc3pi);
			REPROTECT(dBLS1ic = getListElement(calc3, "xtvdvvx"), dBLS1icpi);
			REPROTECT(dBLS2ic = getListElement(calc3, "xtvdvvy"), dBLS2icpi);
		}

		SET_VECTOR_ELT(BLS1s,i,BLS1i);
		SET_VECTOR_ELT(dBLS1sa,i,dBLS1ia);
		if(mod > 6 || nug == 1) SET_VECTOR_ELT(dBLS1sb,i,dBLS1ib);
		if(mod > 6 && nug == 1) SET_VECTOR_ELT(dBLS1sc,i,dBLS1ic);
		SET_VECTOR_ELT(BLS2s,i,BLS2i);
		SET_VECTOR_ELT(dBLS2sa,i,dBLS2ia);
		if(mod > 6 || nug == 1) SET_VECTOR_ELT(dBLS2sb,i,dBLS2ib);
		if(mod > 6 && nug == 1) SET_VECTOR_ELT(dBLS2sc,i,dBLS2ic);
		if(monitor > 2) Rprintf("After calculations\n");
	}
	
	if(monitor > 2) Rprintf("Calculating bhats\n");
	// Now calculate derivative betahat across blocks using Bayesian Generalized Least Squares
	/* Now sum across times to get bhat1 and bhat2 matrices */;
	PROTECT(bhat1 = sumMatrixList(BLS1s));
	PROTECT(dbhat1a = sumMatrixList(dBLS1sa));
	PROTECT(bhat2 = sumVectorList(BLS2s));
	PROTECT(dbhat2a = sumVectorList(dBLS2sa));
	if(mod > 6 || nug == 1)
	{
		PROTECT(dbhat1b = sumMatrixList(dBLS1sb));
		PROTECT(dbhat2b = sumVectorList(dBLS2sb));
	}
	if(mod > 6 && nug == 1)
	{
		PROTECT(dbhat1c = sumMatrixList(dBLS1sc));
		PROTECT(dbhat2c = sumVectorList(dBLS2sc));
	}

	if(monitor > 2) Rprintf("Calculating BGLSmats\n");
	// Now multiply matrices to get derivative
	PROTECT(Sigmacalc = matrixcalc(Sigma,invmethod));
	PROTECT(Sigmainv = getListElement(Sigmacalc,"Vinv"));
	PROTECT(cSigmainv = allocMatrix(REALSXP,betap,betap));
	for(j=0;j<betap;j++)
	{
		for(i=0;i<betap;i++)
		{
			REAL(cSigmainv)[i+j*betap]=REAL(rlambda)[0]*REAL(Sigmainv)[i+j*betap];
		}
	}
	PROTECT(BGLSmat1 = XplusY(bhat1,cSigmainv));
	PROTECT(BGLSmat1inv = cholinvV(BGLSmat1));
	PROTECT(BGLSmat2 = XtimesY(cSigmainv,mu));
	PROTECT(BGLSmat3 = XplusY(bhat2,BGLSmat2));

	if(monitor > 2) Rprintf("Calculating calcs\n");
	// At least one theta i in derivative
	PROTECT(calc1a = XtimesY(BGLSmat1inv,dbhat1a));
	PROTECT(calc2a = XtimesY(calc1a,BGLSmat1inv));
	PROTECT(calc3a = XtimesY(calc2a,BGLSmat3));

	PROTECT(calc4a = XtimesY(BGLSmat1inv,dbhat2a));

	PROTECT(calc5a = XminusY(calc3a,calc4a));

	if(mod > 6 || nug == 1)
	{
		PROTECT(calc1b = XtimesY(BGLSmat1inv,dbhat1b));
		PROTECT(calc2b = XtimesY(calc1b,BGLSmat1inv));
		PROTECT(calc3b = XtimesY(calc2b,BGLSmat3));
		PROTECT(calc4b = XtimesY(BGLSmat1inv,dbhat2b));
		PROTECT(calc5b = XminusY(calc3b,calc4b));
	}
	if(mod > 6 && nug == 1)
	{
		PROTECT(calc1c = XtimesY(BGLSmat1inv,dbhat1c));
		PROTECT(calc2c = XtimesY(calc1c,BGLSmat1inv));
		PROTECT(calc3c = XtimesY(calc2c,BGLSmat3));
		PROTECT(calc4c = XtimesY(BGLSmat1inv,dbhat2c));
		PROTECT(calc5c = XminusY(calc3c,calc4c));
	}

	// Calculate derivative w.r.t. lambda
	PROTECT(dlam1 = XtimesY(BGLSmat1inv,Sigmainv));
	PROTECT(dlam2 = XtimesY(dlam1,BGLSmat1inv));
	PROTECT(dlam3 = XtimesY(dlam2,BGLSmat3));
	PROTECT(dlam4 = XtimesY(dlam1,mu));
	PROTECT(dlam = XminusY(dlam4,dlam3)); 

	// Now set them in dbhat matrix/vector
	if(INTEGER(typeII)[0]==0)
	{
		for(i=0;i<betap;i++)
		{
			REAL(dbhat)[i] = REAL(calc5a)[i];
			if(mod > 6 || nug == 1) REAL(dbhat)[betap+i] = REAL(calc5b)[i];
			if(mod > 6 && nug == 1) REAL(dbhat)[2*betap+i] = REAL(calc5c)[i];
		}
	}
	else
	{
		for(i=0;i<betap;i++)
		{
			REAL(dbhat)[i] = REAL(dlam)[i];
			REAL(dbhat)[betap+i] = REAL(calc5a)[i];
			if(mod > 6 || nug == 1) REAL(dbhat)[2*betap+i] = REAL(calc5b)[i];
			if(mod > 6 && nug == 1) REAL(dbhat)[3*betap+i] = REAL(calc5c)[i];
		}
	}
	// Different unprotects due to calc1* - calc5
	if(length(rpars) == 3) UNPROTECT(76);
	if(length(rpars) == 2) UNPROTECT(69);
	if(length(rpars) == 1) UNPROTECT(62);

	return(dbhat);
}
