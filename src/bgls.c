#include "bjl.h"

/****************************************************************
 * There are two functions here:
 *
 * The first  function calculates the Bayesian Generalized Least 
 * Squares estimate and

 * This second function calculates the Ridge Bayesian Generalized 
 * Least Squares estimate
 ***************************************************************/

/* This is just a wrapper to rbgls with lambda=1 */
SEXP bgls(SEXP spatpars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
	  SEXP vars, SEXP mu, SEXP Sigma, SEXP model, SEXP indexstarts, 
	  SEXP nugget, SEXP invmethod)
{
	SEXP lambda, typeII, result;

	PROTECT(lambda=allocVector(REALSXP,1));
	PROTECT(typeII=allocVector(LGLSXP,1));
	REAL(lambda)[0]=1.0;
	INTEGER(typeII)[0]=0;

	PROTECT(result = rbgls(spatpars, lambda, y, X, coords, Dscale, vars, 
			       mu, Sigma, model, indexstarts, nugget, invmethod, typeII));

	UNPROTECT(3);
	return(result);
}


SEXP rbgls(SEXP spatpars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
	   SEXP vars, SEXP mu, SEXP Sigma, SEXP model, SEXP indexstarts, 
	   SEXP nugget, SEXP invmethod, SEXP typeII)
{
	// Variables for calculating estimate in block diagonal form
	SEXP yi, Xi, coordsi, varsi, Vi;
	PROTECT_INDEX yipi, Xipi, coordsipi, varsipi, Vipi; 

	// Block diagonal using Bayesian Generalized Least Squares
	SEXP calc, BLS1i, BLS2i, BLS1s, BLS2s, bhat1, bhat2, bhat;
	PROTECT_INDEX calcpi, BLS1ipi, BLS2ipi;

	// Matrices used in calculating the final betahat
	SEXP BGLSmat1, BGLSmat1inv, BGLSmat2, BGLSmat3, Sigmacalc, Sigmainv, cSigmainv;

	// Dims for block diagonal work
	SEXP r1, r2, XDIMS, p1, p2, coordp1, coordp2; 

	SEXP nis, rpars, rlambda; //Get nis to find out largest one in block diagonal
	SEXP result, resultnames;
	int maxni, nug;

	int N, T, betap;
	int ni, start, stop;
	int i, j;
	int monitor=0; // Set from 1-3 for levels of monitoring

	/* If using typeII method then get lambda from first element of pars */;
	PROTECT(rlambda = allocVector(REALSXP,1));
	if(INTEGER(typeII)[0]==1)
	{
		PROTECT(rpars = allocVector(REALSXP,length(spatpars)-1));
		REAL(rlambda)[0]=REAL(spatpars)[0];
		for(i=0;i<length(rpars);i++) REAL(rpars)[i]=REAL(spatpars)[i+1];
	}
	else
	{
		PROTECT(rpars = allocVector(REALSXP,length(spatpars)));
		for(i=0;i<length(rpars);i++) REAL(rpars)[i]=REAL(spatpars)[i];
		REAL(rlambda)[0] = REAL(lambda)[0];
	}

	T = length(indexstarts); // Get vector of starting points
	N = length(y);
	nug = INTEGER(nugget)[0];

	if(monitor > 2) Rprintf("T is %d and N is %d\n",T,N);

	PROTECT(r1 = allocVector(INTSXP,1)); 
	PROTECT(r2 = allocVector(INTSXP,1));
	PROTECT(XDIMS = getAttrib(X, R_DimSymbol));
	PROTECT(p1 = allocVector(INTSXP,1)); INTEGER(p1)[0] = 0;
	PROTECT(p2 = allocVector(INTSXP,1)); INTEGER(p2)[0] = INTEGER(XDIMS)[1]-1;
	PROTECT(coordp1 = allocVector(INTSXP,1)); INTEGER(coordp1)[0]=0;
	PROTECT(coordp2 = allocVector(INTSXP,1)); INTEGER(coordp2)[0]=1;
	betap = INTEGER(XDIMS)[1];

	/* Find maximum ni to allocate matrices */;
	PROTECT(nis = allocVector(INTSXP,T));
	if(T > 0)
	{
		for(i=0;i<T-1;i++) INTEGER(nis)[i] = INTEGER(indexstarts)[i+1]-INTEGER(indexstarts)[i];
		INTEGER(nis)[T-1] = N - INTEGER(indexstarts)[T-1]+1;
		R_isort(INTEGER(nis), T);
		maxni = INTEGER(nis)[T-1];
	}
	else maxni = N;

	PROTECT(BLS1s = allocVector(VECSXP, T));
	PROTECT(BLS2s = allocVector(VECSXP, T));

	/* Initialize dummy objects before being used in loop */;
	PROTECT_WITH_INDEX(yi = allocVector(REALSXP,maxni), &yipi);
	PROTECT_WITH_INDEX(Xi = allocMatrix(REALSXP,maxni,maxni), &Xipi);
	PROTECT_WITH_INDEX(coordsi = allocMatrix(REALSXP,maxni,2), &coordsipi);
	PROTECT_WITH_INDEX(varsi = allocVector(REALSXP,maxni), &varsipi);
	PROTECT_WITH_INDEX(Vi = allocMatrix(REALSXP,maxni,maxni), &Vipi);
	PROTECT_WITH_INDEX(calc = allocVector(VECSXP, 2), &calcpi);
	PROTECT_WITH_INDEX(BLS1i = allocMatrix(REALSXP,betap,betap), &BLS1ipi);
	PROTECT_WITH_INDEX(BLS2i = allocVector(REALSXP,betap), &BLS2ipi);

	/* Now loop through index of times */;
	for(i=0; i<T; i++)
	{
		/* If in the index for vector of observations */;
		if(T > 1)
		{
			start=INTEGER(indexstarts)[i]-1;
			stop=INTEGER(indexstarts)[i+1]-2;
			ni=stop-start+1;
		}
		/* If on the last set or if only one set */;
		if(i == T-1)
		{
			if(T==1) start=0;
			else start=INTEGER(indexstarts)[i]-1;
			stop=N-1;
			ni=stop-start+1;
		}
		
		if(monitor > 2) Rprintf("Tracking loop\n\ti is %d\n\tstart is %d\n\tstop is %d\n\tni is %d\n",i,start,stop,ni);
		
		REPROTECT(yi = allocVector(REALSXP,ni), yipi);
		REPROTECT(Xi = allocMatrix(REALSXP,ni,ni), Xipi);
		REPROTECT(coordsi = allocMatrix(REALSXP,ni,2), coordsipi);
		REPROTECT(varsi = allocVector(REALSXP,ni), varsipi);
		REPROTECT(Vi = allocMatrix(REALSXP,ni,ni), Vipi);
		REPROTECT(calc = allocVector(VECSXP, 2), calcpi);
		REPROTECT(BLS1i = allocMatrix(REALSXP,betap,betap), BLS1ipi);
		REPROTECT(BLS2i = allocVector(REALSXP,betap), BLS2ipi);
		if(monitor > 2) Rprintf("After reprotecting block data\n");
		
		INTEGER(r1)[0] = start;
		INTEGER(r2)[0] = stop;
		
		for(j=0;j<ni;j++) REAL(yi)[j] = REAL(y)[start+j];
		REPROTECT(Xi = subMatrix(X,r1,r2,p1,p2), Xipi);
		if(monitor > 2) Rprintf("Before coordsi\n");
		REPROTECT(coordsi = subMatrix(coords,r1,r2,coordp1,coordp2), coordsipi);
		if(monitor > 2) Rprintf("After coordsi\n");

		for(j=0;j<ni;j++) REAL(varsi)[j] = REAL(vars)[start+j];
		REPROTECT(Vi = setV(rpars,model,nugget,coordsi,varsi,Dscale),Vipi);

		REPROTECT(calc = bglscalc(Xi,Vi,yi,invmethod),calcpi);
		if(monitor > 2) Rprintf("After bglscalc\n");
		REPROTECT(BLS1i = getListElement(calc,"xtvx"), BLS1ipi);
		REPROTECT(BLS2i = getListElement(calc,"xtvy"), BLS2ipi);
		SET_VECTOR_ELT(BLS1s,i,BLS1i);
		SET_VECTOR_ELT(BLS2s,i,BLS2i);
		if(monitor > 2) Rprintf("After calculations\n");
	}
	
	// Now calculate betahat across blocks using Bayesian Generalized Least Squares
	/* Now sum across times to get bhat1 and bhat2 matrices */;
	PROTECT(bhat1 = sumMatrixList(BLS1s));
	PROTECT(bhat2 = sumVectorList(BLS2s));

	PROTECT(Sigmacalc = matrixcalc(Sigma,invmethod));
	PROTECT(Sigmainv = getListElement(Sigmacalc,"Vinv"));
	PROTECT(cSigmainv = allocMatrix(REALSXP,betap,betap));
	for(j=0;j<betap;j++)
	{
		for(i=0;i<betap;i++)
		{
			REAL(cSigmainv)[i+betap*j] = REAL(Sigmainv)[i+betap*j]*REAL(rlambda)[0];
		}
	}
	if(monitor > 2) Rprintf("After prior covariance calculation\n");
	PROTECT(BGLSmat1 = XplusY(bhat1,cSigmainv));
	PROTECT(BGLSmat1inv = svdinvV(BGLSmat1));
	PROTECT(BGLSmat2 = XtimesY(cSigmainv,mu));
	PROTECT(BGLSmat3 = XplusY(bhat2,BGLSmat2));
	PROTECT(bhat = XtimesY(BGLSmat1inv,BGLSmat3));
	if(monitor > 2) Rprintf("After BGLS calculations\n");

	// Allocate list and names for both beta and covariance
	PROTECT(result = allocVector(VECSXP, 2));
	PROTECT(resultnames = allocVector(STRSXP, 2));
	setAttrib(result, R_NamesSymbol, resultnames);
	
	// Name elements in list
	SET_STRING_ELT(resultnames,0,mkChar("beta"));
	SET_STRING_ELT(resultnames,1,mkChar("cov"));
	
	SET_VECTOR_ELT(result,0,bhat);
	SET_VECTOR_ELT(result,1,BGLSmat1inv);
	
	UNPROTECT(32);
	return(result);
	
}

