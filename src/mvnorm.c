#include "bjl.h"

/*************************************************************
 * For the model y~N(Xbeta,V)
 *
 * mvnorm is a function to give a list of the log of the
 * determinant of V and the Gaussian kernel
 * (y-Xbeta)^TV^{-1}(y-Xbeta)
 *
 * For the derivative of V with respect to variance parameter
 * theta_i dV/dtheta_i, then dmvnorm gives a list of the 
 * trace of Vinv*dV the derivative of the kernel 
 * -(y-Xbeta)^TV^{-1}dVV^{-1}(y-Xbeta)
 *
 * Also includes functions that are used for the calculations 
 * in the aposteriori, daposteriori, rblgs, drbgls, 
 * raposteriori and draposteriori functions
 *
 *************************************************************/

SEXP mvnorm(SEXP y, SEXP X, SEXP beta, SEXP V, SEXP invmethod)
{
	SEXP result, names, logdet, kern;
	SEXP yhat, ss, ssT, ss1, Vcalc, Vinv; // For calculations
	int monitor=0;

	if(monitor>2) Rprintf("allocating results\n");
	// Setup R list for results, and label results
	PROTECT(result = allocVector(VECSXP, 2));
	PROTECT(names = allocVector(STRSXP, 2));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("logdet"));
	SET_STRING_ELT(names,1,mkChar("kern"));

	if(monitor>2) Rprintf("doing matrixcalci\n");
	// Calculate Vinv and logdet in matrixcalc
	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));
	PROTECT(logdet = getListElement(Vcalc, "logdet"));

	if(monitor>2) Rprintf("calculating kernel\n");
	// Calculate kernel (y-Xbeta)^TV^{-1}(y-Xbeta)
	PROTECT(yhat = XtimesY(X,beta));
	PROTECT(ss = XminusY(y,yhat));

	PROTECT(ssT = tX(ss));
	PROTECT(ss1 = XtimesY(ssT,Vinv));
	PROTECT(kern = XtimesY(ss1,ss));

	SET_VECTOR_ELT(result, 0, logdet);
	SET_VECTOR_ELT(result, 1, kern);
	
	UNPROTECT(10);
	return(result);
}

SEXP dmvnorm(SEXP y, SEXP X, SEXP beta, SEXP V, SEXP dV, SEXP invmethod)
{
	SEXP result, names, trace, dkern;
	SEXP Vcalc, Vinv, VinvdV, yhat, ss1, ss1T, ss2, ss3, ss4;

	// Setup R list for results, and label results
	PROTECT(result = allocVector(VECSXP, 2));
	PROTECT(names = allocVector(STRSXP, 2));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("trace"));
	SET_STRING_ELT(names,1,mkChar("dkern"));

	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));
	PROTECT(VinvdV = XtimesY(Vinv,dV));
	PROTECT(trace = tr(VinvdV));

	PROTECT(yhat = XtimesY(X,beta));
	PROTECT(ss1 = XminusY(y,yhat));
	// PROTECT(ss2 = XTtimesY(ss1,Vinv)); // Problem with XTTimesY
	PROTECT(ss1T = tX(ss1));
	PROTECT(ss2 = XtimesY(ss1T,Vinv));
	PROTECT(ss3 = XtimesY(ss2,dV));
	PROTECT(ss4 = XtimesY(ss3,Vinv));
	PROTECT(dkern = XtimesY(ss4,yhat));

	REAL(dkern)[0]=-REAL(dkern)[0];

	SET_VECTOR_ELT(result, 0, trace);
	SET_VECTOR_ELT(result, 1, dkern);

	UNPROTECT(13);
	return(result);
}

// Calculate "xtvx" X^TV^{-1}X and "xtvy" X^TV^{-1}y 
SEXP bglscalc(SEXP X, SEXP V, SEXP y, SEXP invmethod)
{
	SEXP result, names;
	SEXP Vcalc, Vinv, XT, XTV, xtvx, xtvy;

	PROTECT(result = allocVector(VECSXP, 2));
	PROTECT(names = allocVector(STRSXP, 2));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("xtvx"));
	SET_STRING_ELT(names,1,mkChar("xtvy"));
	
	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));
	PROTECT(XT = tX(X));
	PROTECT(XTV = XtimesY(XT,Vinv));

	PROTECT(xtvx = XtimesY(XTV,X));
	PROTECT(xtvy = XtimesY(XTV,y));

	SET_VECTOR_ELT(result,0,xtvx);
	SET_VECTOR_ELT(result,1,xtvy);

	UNPROTECT(8);
	return(result);
}

// Calcualte "xtvx" X^TV^{-1}X, "xtvdvvx" X^TV^{-1}dVV^{-1}X
// "xtvy" X^TV^{-1}y, "xtvdvvy" X^TV^{-1}dVV^{-1}y 
SEXP dbglscalc(SEXP X, SEXP V, SEXP dV, SEXP y, SEXP invmethod)
{
	SEXP result, names;
	SEXP Vcalc, Vinv, XT, XTV, XTVdV, XTVdVV;
	SEXP xtvx, xtvdvvx, xtvy, xtvdvvy;

	PROTECT(result = allocVector(VECSXP,4));
	PROTECT(names = allocVector(STRSXP,4));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("xtvx"));
	SET_STRING_ELT(names,1,mkChar("xtvdvvx"));
	SET_STRING_ELT(names,2,mkChar("xtvy"));
	SET_STRING_ELT(names,3,mkChar("xtvdvvy"));

	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));

	PROTECT(XT = tX(X));
	PROTECT(XTV = XtimesY(XT,Vinv));
	PROTECT(xtvx = XtimesY(XTV,X));
	PROTECT(XTVdV = XtimesY(XTV,dV));
	PROTECT(XTVdVV = XtimesY(XTVdV,Vinv));
	PROTECT(xtvdvvx = XtimesY(XTVdVV,X));
	PROTECT(xtvy = XtimesY(XTV,y));
	PROTECT(xtvdvvy = XtimesY(XTVdVV,y));

	SET_VECTOR_ELT(result,0,xtvx);
	SET_VECTOR_ELT(result,1,xtvdvvx);
	SET_VECTOR_ELT(result,2,xtvy);
	SET_VECTOR_ELT(result,3,xtvdvvy);

	UNPROTECT(12);
	return(result);
}

// Calculate "trvdv" tr(V^{-1}dV), 
// "dkern" (y-Xbeta)^TV^{-1}dVV^{-1}(y-Xbeta)
// "ssdbeta" (y-Xbeta)^TV^{-1}Xdbeta
SEXP dapostcalc(SEXP X, SEXP V, SEXP dV, SEXP beta, SEXP dbeta, 
		SEXP y, SEXP invmethod)
{
	SEXP result, names;
	SEXP Vcalc, Vinv, yhat, ss, ssT, ssTV, VdV, ssVdV, ssVdVV, ssTVX;
	SEXP trvdv, dkern, ssdbeta;

	PROTECT(result = allocVector(VECSXP,3));
	PROTECT(names = allocVector(STRSXP,3));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("trvdv"));
	SET_STRING_ELT(names,1,mkChar("dkern"));
	SET_STRING_ELT(names,2,mkChar("ssdbeta"));

	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));

	PROTECT(yhat = XtimesY(X,beta));
	PROTECT(ss = XminusY(y,yhat));
	PROTECT(ssT = tX(ss));
	PROTECT(ssTV = XtimesY(ssT,Vinv));
	PROTECT(VdV = XtimesY(Vinv,dV));
	PROTECT(trvdv = tr(VdV));
	PROTECT(ssVdV = XtimesY(ssT,VdV));
	PROTECT(ssVdVV = XtimesY(ssVdV,Vinv));
	PROTECT(ssTVX = XtimesY(ssTV,X));
	PROTECT(dkern = XtimesY(ssVdVV,ss));
	PROTECT(ssdbeta = XtimesY(ssTVX,dbeta));

	SET_VECTOR_ELT(result,0,trvdv);
	SET_VECTOR_ELT(result,1,dkern);
	SET_VECTOR_ELT(result,2,ssdbeta);

	UNPROTECT(15);
	return(result);
}

SEXP rapostcalc(SEXP y, SEXP X, SEXP beta, SEXP V, SEXP invmethod)
{
	SEXP result, names, logdet, kern;
	SEXP yhat, ss, ssT, ss1, Vcalc, Vinv; // For calculations
	SEXP XT, XTV, xtvx;
	int monitor=0;

	if(monitor>2) Rprintf("allocating results\n");
	// Setup R list for results, and label results
	PROTECT(result = allocVector(VECSXP, 3));
	PROTECT(names = allocVector(STRSXP, 3));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("logdet"));
	SET_STRING_ELT(names,1,mkChar("kern"));
	SET_STRING_ELT(names,2,mkChar("xtvx"));

	if(monitor>2) Rprintf("doing matrixcalci\n");
	// Calculate Vinv and logdet in matrixcalc
	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));
	PROTECT(logdet = getListElement(Vcalc, "logdet"));

	if(monitor>2) Rprintf("calculating kernel\n");
	// Calculate kernel (y-Xbeta)^TV^{-1}(y-Xbeta)
	PROTECT(yhat = XtimesY(X,beta));
	PROTECT(ss = XminusY(y,yhat));

	PROTECT(ssT = tX(ss));
	PROTECT(ss1 = XtimesY(ssT,Vinv));
	PROTECT(kern = XtimesY(ss1,ss));

	PROTECT(XT = tX(X));
	PROTECT(XTV = XtimesY(XT,Vinv));
	PROTECT(xtvx = XtimesY(XTV,X));

	SET_VECTOR_ELT(result, 0, logdet);
	SET_VECTOR_ELT(result, 1, kern);
	SET_VECTOR_ELT(result, 2, xtvx);
	
	UNPROTECT(13);
	return(result);
}

// Calculate "trvdv" tr(V^{-1}dV), 
// "dkern" (y-Xbeta)^TV^{-1}dVV^{-1}(y-Xbeta)
// "ssdbeta" (y-Xbeta)^TV^{-1}Xdbeta
// "xtvx" X^TV^{-1}X
// "xtdvvx" X^TV^{-1}dVV^{-1}X
SEXP drapostcalc(SEXP X, SEXP V, SEXP dV, SEXP beta, SEXP dbeta, 
		SEXP y, SEXP invmethod)
{
	SEXP result, names;
	SEXP Vcalc, Vinv, yhat, ss, ssT, ssTV, VdV, ssVdV, ssVdVV, ssTVX,
		XT, XTV, XTVdV, XTVdVV;
	SEXP trvdv, dkern, ssdbeta, xtvx, xtvdvvx;

	PROTECT(result = allocVector(VECSXP,5));
	PROTECT(names = allocVector(STRSXP,5));
	setAttrib(result, R_NamesSymbol, names);
	SET_STRING_ELT(names,0,mkChar("trvdv"));
	SET_STRING_ELT(names,1,mkChar("dkern"));
	SET_STRING_ELT(names,2,mkChar("ssdbeta"));
	SET_STRING_ELT(names,3,mkChar("xtvx"));
	SET_STRING_ELT(names,4,mkChar("xtvdvvx"));

	PROTECT(Vcalc = matrixcalc(V,invmethod));
	PROTECT(Vinv = getListElement(Vcalc, "Vinv"));

	PROTECT(yhat = XtimesY(X,beta));
	PROTECT(ss = XminusY(y,yhat));
	PROTECT(ssT = tX(ss));
	PROTECT(ssTV = XtimesY(ssT,Vinv));
	PROTECT(VdV = XtimesY(Vinv,dV));
	PROTECT(trvdv = tr(VdV));
	PROTECT(ssVdV = XtimesY(ssT,VdV));
	PROTECT(ssVdVV = XtimesY(ssVdV,Vinv));
	PROTECT(ssTVX = XtimesY(ssTV,X));
	PROTECT(dkern = XtimesY(ssVdVV,ss));
	PROTECT(ssdbeta = XtimesY(ssTVX,dbeta));
	PROTECT(XT = tX(X));
	PROTECT(XTV = XtimesY(XT,Vinv));
	PROTECT(XTVdV = XtimesY(XTV,dV));
	PROTECT(XTVdVV = XtimesY(XTVdV,Vinv));
	PROTECT(xtvx = XtimesY(XTV,X));
	PROTECT(xtvdvvx = XtimesY(XTVdVV,X));

	SET_VECTOR_ELT(result,0,trvdv);
	SET_VECTOR_ELT(result,1,dkern);
	SET_VECTOR_ELT(result,2,ssdbeta);
	SET_VECTOR_ELT(result,3,xtvx);
	SET_VECTOR_ELT(result,4,xtvdvvx);

	UNPROTECT(21);
	return(result);
}

