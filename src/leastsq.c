#include "bjl.h"

/* Give negative log likelihood of multivariate normal distribution */

SEXP leastsq(SEXP beta, SEXP y, SEXP X, SEXP V)
{
	int n;
	SEXP det, lkhd, kernel;

	n = length(y);
	PROTECT(lkhd = allocVector(REALSXP,1));
	PROTECT(kernel = allocVector(REALSXP,1));

	PROTECT(det = detX(V));
	kernel = XtimesY(XtimesY(tX(XminusY(y,XtimesY(X,beta))),cholinvV(V)),XminusY(y,XtimesY(X,beta)));
	REAL(lkhd)[0] = log(fabs(REAL(det)[0])) + REAL(kernel)[0];

	UNPROTECT(3);
	return(lkhd);
}
