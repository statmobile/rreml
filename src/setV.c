#include "bjl.h"

/***********************************************
 *
 * This is C code to set the covariance matrix
 * by calculating the geodesic distances and 
 * incorporating the prescibed variance for
 * each point.
 * 
 * There is also the code to create the 
 * derivative of the covariance matrix
 *
 * Future implementations would be to give 
 * different distance calculation options
 *
 ***********************************************/

SEXP setV(SEXP spatpars, SEXP model, SEXP nugget, SEXP coords, 
	  SEXP vars, SEXP Dscale)
{
	SEXP DIMS, D, C, V;
	int i, j, n;

	PROTECT(DIMS = getAttrib(coords, R_DimSymbol));
	n = INTEGER(DIMS)[0];

	PROTECT(V = allocMatrix(REALSXP,n,n));

	// Calculate distance based on coordinates, then scale
	PROTECT(D = geodist(coords, coords));
	for(j=0;j<n;j++)
	{
		for(i=0;i<n;i++)
		{
			REAL(D)[i+n*j]=REAL(D)[i+n*j]/REAL(Dscale)[0];
		}
	}
	// Set spatial correlations
	PROTECT(C = spatCOV(D,spatpars,model,nugget));
	// Incorporate variances
	for(j=0;j<n;j++)
	{
		for(i=0;i<n;i++)
		{
			REAL(V)[i+n*j]=REAL(C)[i+n*j] *
				sqrt(REAL(vars)[i]) * 
				sqrt(REAL(vars)[j]);
		}
	}

	UNPROTECT(4);
	return(V);
}										   

SEXP setdV(SEXP di, SEXP spatpars, SEXP model, SEXP nugget, SEXP coords, 
	   SEXP vars, SEXP Dscale)
{
	SEXP DIMS, D, dC, dV;
	int i, j, n;

	PROTECT(DIMS = getAttrib(coords, R_DimSymbol));
	n = INTEGER(DIMS)[0];

	PROTECT(dV = allocMatrix(REALSXP,n,n));

	// Calculate distance based on coordinates, then scale
	PROTECT(D = geodist(coords, coords));
	for(j=0;j<n;j++)
	{
		for(i=0;i<n;i++)
		{
			REAL(D)[i+n*j]=REAL(D)[i+n*j]/REAL(Dscale)[0];
		}
	}
	// Set spatial correlations
	PROTECT(dC = dspatCOV(di,D,spatpars,model,nugget));
	// Incorporate variances
	for(j=0;j<n;j++)
	{
		for(i=0;i<n;i++)
		{
			REAL(dV)[i+n*j]=REAL(dC)[i+n*j] *
				sqrt(REAL(vars)[i]) * 
				sqrt(REAL(vars)[j]);
		}
	}

	UNPROTECT(4);
	return(dV);
}										   
