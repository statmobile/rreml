#include "bjl.h"

/****************************************************************
 * This function is the derivative of raposteriori with respect 
 * to the spatial parameters
 ***************************************************************/


SEXP draposteriori(SEXP pars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
		   SEXP vars, SEXP model, SEXP indexstarts, SEXP mu, 
		   SEXP Sigma, SEXP nugget, SEXP invmethod, SEXP typeII)
{
	// General data for calculations
	SEXP beta, spatpars, rpars, rlambda;
	// For block diagonal work
	SEXP Xi, coordsi, calc1, calc2, calc3, varsi, Vi, yi, 
		dVi1, dVi2, dVi3, xtvxi, xtvdv1vxi, xtvdv2vxi, xtvdv3vxi;
	// To index the reprotected SEXP objects
	PROTECT_INDEX Xipi, coordsipi, calc1pi, calc2pi, calc3pi, 
		varsipi, Vipi, yipi, dVi1pi, dVi2pi, dVi3pi,
		xtvxipi, xtvdv1vxipi, xtvdv2vxipi, xtvdv3vxipi;
	// For step by step calculations
      	SEXP ss1a, ss1b, ss1c, ss2a, ss2b, ss2c, trVdVa, trVdVb, trVdVc;
	// To index the reprotected SEXP objects
	PROTECT_INDEX ss1api, ss1bpi, ss1cpi, ss2api, ss2bpi, ss2cpi, 
		trVdVapi, trVdVbpi, trVdVcpi;
	// Dims for block diagonal work
	SEXP r1, r2, XDIMS, p1, p2, coordp1, coordp2; 
	// Sum up X^TV^{-1}X and add cSig^{-1} then invert
	SEXP xtvxs, xtvdv1vxs, xtvdv2vxs, xtvdv3vxs, sumxtvx, sumxtvdv1vx, sumxtvdv2vx, sumxtvdv3vx, 
		xtvxpluscSiginv, xtvxcSiginv, xtvxcSiginvdv1, xtvxcSiginvdv2, xtvxcSiginvdv3, 
		trxtvdv1, trxtvdv2, trxtvdv3;
	// derivative of aposteriori
	SEXP deriv, derivsa, derivsb, derivsc;
	// derivative of prior information
	SEXP Sigmacalc, Sigmainv, cSiginv, dpriorcalc1, dpriorcalc2, dpriorcalc3, dprior1, dprior2, dprior3;
	// Derivative of Bayesian GLS
	SEXP dbeta, dbeta1, dbeta2, dbeta3;
	// Derivative wrt spatial parameter
	SEXP thetai1, thetai2, thetai3;
	// Variables for calculating derivative w.r.t. lambda
	SEXP dblambda, dlam1, dlam2, dlam3, trdlam, lamcalc, ssdlami, ssdlams;
	PROTECT_INDEX lamcalcpi, ssdlamipi;

	SEXP nis; //Get nis to find out largest one
	SEXP BGLSres; // To get results of BGLS

	int derivlen=1;
	int maxni, nug;  // Allocate maximum matrix
	int betap, spatp=1, N, mod, T, p;
	int ni, start, stop;
	int i, j;
	int monitor=0; // Set from 1-3 for levels of monitoring

	/* If using typeII method then get lambda from first element of pars */;
	PROTECT(rlambda = allocVector(REALSXP,1));
	if(INTEGER(typeII)[0]==1)
	{
		PROTECT(rpars = allocVector(REALSXP,length(pars)-1));
		REAL(rlambda)[0]=REAL(pars)[0];
		for(i=0;i<length(rpars);i++) REAL(rpars)[i]=REAL(pars)[i+1];
	}
	else
	{
		PROTECT(rpars = allocVector(REALSXP,length(pars)));
		for(i=0;i<length(rpars);i++) REAL(rpars)[i]=REAL(pars)[i];
		REAL(rlambda)[0] = REAL(lambda)[0];
	}

	mod = INTEGER(model)[0];  // Get spatial model
	T = length(indexstarts); // Get vector of starting points
	N = length(y);
	nug = INTEGER(nugget)[0];

	PROTECT(r1 = allocVector(INTSXP,1)); 
	PROTECT(r2 = allocVector(INTSXP,1));
	PROTECT(XDIMS = getAttrib(X, R_DimSymbol));
	PROTECT(p1 = allocVector(INTSXP,1)); INTEGER(p1)[0] = 0;
	PROTECT(p2 = allocVector(INTSXP,1)); INTEGER(p2)[0] = INTEGER(XDIMS)[1]-1;
	PROTECT(coordp1 = allocVector(INTSXP,1)); INTEGER(coordp1)[0]=0;
	PROTECT(coordp2 = allocVector(INTSXP,1)); INTEGER(coordp2)[0]=1;

	p = INTEGER(XDIMS)[1];

	// Set length of derivative
	if(mod > 6 || nug == 1) derivlen=derivlen+1;
	if(mod > 6 && nug == 1) derivlen=derivlen+1;
	if(INTEGER(typeII)[0]==1) derivlen=derivlen+1;

	PROTECT(deriv=allocVector(REALSXP, derivlen));
	if(monitor > 2) Rprintf("derivlen is %d\n",derivlen);

	PROTECT(derivsa = allocVector(REALSXP,T));
	PROTECT(derivsb = allocVector(REALSXP,T));
	PROTECT(derivsc = allocVector(REALSXP,T));

	// Set index of derivatives
	PROTECT(thetai1 = allocVector(INTSXP,1));
	INTEGER(thetai1)[0] = 1;
	PROTECT(thetai2 = allocVector(INTSXP,1));
	INTEGER(thetai2)[0] = 2;
	PROTECT(thetai3 = allocVector(INTSXP,1));
	INTEGER(thetai3)[0] = 3;

	// Set up sum of variance calculations
	PROTECT(xtvxs = allocVector(VECSXP,T));
	PROTECT(xtvdv1vxs = allocVector(VECSXP,T));
	PROTECT(xtvdv2vxs = allocVector(VECSXP,T));
	PROTECT(xtvdv3vxs = allocVector(VECSXP,T));
	PROTECT(ssdlams = allocVector(REALSXP,T));

	/* Find maximum ni to allocate matrices */;
	PROTECT(nis = allocVector(INTSXP,T));
	if(T > 0)
	{
		for(i=0;i<T-1;i++) INTEGER(nis)[i] = INTEGER(indexstarts)[i+1]-INTEGER(indexstarts)[i];
		INTEGER(nis)[T-1] = N - INTEGER(indexstarts)[T-1]+1;
		R_isort(INTEGER(nis), T);
		maxni = INTEGER(nis)[T-1];
	}
	else maxni = N;

	/* Print rpars to keep track of everything when monitoring*/;
	if(monitor > 0)
	{
		if(mod>6 || nug ==1)
		{
			if(mod>6 && nug==1)
			{
				Rprintf("%e\t%e\t%e\n",REAL(rpars)[length(rpars)-3],
					REAL(rpars)[length(rpars)-2],
					REAL(rpars)[length(rpars)-1]);
			}
			else
			{
				Rprintf("%e\t%e\n",REAL(rpars)[length(rpars)-2],REAL(rpars)[length(rpars)-1]);
			}
		}
		else Rprintf("%e\n",REAL(rpars)[length(rpars)-1]);
	}

	/* Set parameters for model */;
	if(mod > 6 || nug == 1) spatp=spatp+1;
	if(mod > 6 && nug == 1) spatp=spatp+1;
	PROTECT(spatpars = allocVector(REALSXP, spatp));
	if(mod <= 6) // For one parameter models
	{
		// Use BGLS (Will set beta=bhat in if statement later)
		betap = INTEGER(XDIMS)[1];
		REAL(spatpars)[0] = REAL(rpars)[0];
		// If we have nugget:sill ratio
		if(nug == 1 )
		{
			REAL(spatpars)[1] = REAL(rpars)[1];
		}
	}
	if(mod > 6 && mod <= 9) // For two parameter models
	{
		// Use BGLS (Will set beta=bhat in if statement later)
		betap = INTEGER(XDIMS)[1];
		REAL(spatpars)[0] = REAL(rpars)[0];
		REAL(spatpars)[1] = REAL(rpars)[1];
		// If we have nugget:sill ratio
		if(nug == 1)
		{
			REAL(spatpars)[2] = REAL(rpars)[2];
		}
	}

	if(monitor > 2) Rprintf("Before allocating matrices\n");

	/* Initialize dummy objects before being used in loop */;
	PROTECT_WITH_INDEX(yi = allocVector(REALSXP,maxni), &yipi);
	PROTECT_WITH_INDEX(Xi = allocMatrix(REALSXP,maxni,maxni), &Xipi);
	PROTECT_WITH_INDEX(coordsi = allocMatrix(REALSXP,maxni,2), &coordsipi);
	PROTECT_WITH_INDEX(varsi = allocVector(REALSXP,maxni), &varsipi);
	PROTECT_WITH_INDEX(Vi = allocMatrix(REALSXP,maxni,maxni), &Vipi);
	PROTECT_WITH_INDEX(dVi1 = allocMatrix(REALSXP,maxni,maxni), &dVi1pi);
	PROTECT_WITH_INDEX(dVi2 = allocMatrix(REALSXP,maxni,maxni), &dVi2pi);
	PROTECT_WITH_INDEX(dVi3 = allocMatrix(REALSXP,maxni,maxni), &dVi3pi);
	PROTECT_WITH_INDEX(xtvxi = allocMatrix(REALSXP,betap,betap), &xtvxipi);
	PROTECT_WITH_INDEX(xtvdv1vxi = allocMatrix(REALSXP,betap,betap), &xtvdv1vxipi);
	PROTECT_WITH_INDEX(xtvdv2vxi = allocMatrix(REALSXP,betap,betap), &xtvdv2vxipi);
	PROTECT_WITH_INDEX(xtvdv3vxi = allocMatrix(REALSXP,betap,betap), &xtvdv3vxipi);
	PROTECT_WITH_INDEX(lamcalc = allocVector(VECSXP,5), &lamcalcpi);
	PROTECT_WITH_INDEX(ssdlami = allocVector(REALSXP,1), &ssdlamipi);
	PROTECT_WITH_INDEX(calc1 = allocVector(VECSXP,5), &calc1pi);
	PROTECT_WITH_INDEX(calc2 = allocVector(VECSXP,5), &calc2pi);
	PROTECT_WITH_INDEX(calc3 = allocVector(VECSXP,5), &calc3pi);
	PROTECT_WITH_INDEX(ss1a = allocVector(REALSXP,1), &ss1api);
	PROTECT_WITH_INDEX(ss1b = allocVector(REALSXP,1), &ss1bpi);
	PROTECT_WITH_INDEX(ss1c = allocVector(REALSXP,1), &ss1cpi);
	PROTECT_WITH_INDEX(ss2a = allocVector(REALSXP,1), &ss2api);
	PROTECT_WITH_INDEX(ss2b = allocVector(REALSXP,1), &ss2bpi);
	PROTECT_WITH_INDEX(ss2c = allocVector(REALSXP,1), &ss2cpi);
	PROTECT_WITH_INDEX(trVdVa = allocVector(REALSXP,1), &trVdVapi);
	PROTECT_WITH_INDEX(trVdVb = allocVector(REALSXP,1), &trVdVbpi);
	PROTECT_WITH_INDEX(trVdVc = allocVector(REALSXP,1), &trVdVcpi);
	if(monitor > 2) Rprintf("After allocating matrices\n");

	/* Use derivative of ridge BGLS */;
	if(monitor > 2) Rprintf("Before call to bgls\n");
	PROTECT(BGLSres=rbgls(pars,rlambda,y,X,coords,Dscale,vars,mu,Sigma,model,
			      indexstarts,nugget,invmethod,typeII));
	PROTECT(beta=getListElement(BGLSres, "beta"));
	if(monitor > 2) Rprintf("After call to bgls\n");
	PROTECT(dbeta=drbgls(pars,rlambda,y,X,coords,Dscale,vars,mu,Sigma,model,
			     indexstarts,nugget,invmethod,typeII));
	if(monitor > 2) Rprintf("After call to dbgls\n");

	/* Now set each one of the vectors of derivatives*/
	PROTECT(dblambda = allocVector(REALSXP,betap));
	PROTECT(dbeta1 = allocVector(REALSXP,betap));
	PROTECT(dbeta2 = allocVector(REALSXP,betap));
	PROTECT(dbeta3 = allocVector(REALSXP,betap));
	if(INTEGER(typeII)[0]==0)
	{
		for(j=0;j<betap;j++)
		{
			REAL(dbeta1)[j]=REAL(dbeta)[j];
			if(mod > 6 || nug == 1) REAL(dbeta2)[j]=REAL(dbeta)[betap+j];
			if(mod > 6 && nug == 1) REAL(dbeta3)[j]=REAL(dbeta)[2*betap+j];
		}
	}
	else
	{
		for(j=0;j<betap;j++)
		{
			REAL(dblambda)[j]=REAL(dbeta)[j];
			REAL(dbeta1)[j]=REAL(dbeta)[betap+j];
			if(mod > 6 || nug == 1) REAL(dbeta2)[j]=REAL(dbeta)[2*betap+j];
			if(mod > 6 && nug == 1) REAL(dbeta3)[j]=REAL(dbeta)[3*betap+j];
		}
	}

	/* Now loop through index of times */;
	for(i=0; i<T; i++)
	{
		/* If in the index for vector of observations */;
		if(T > 1)
		{
			start=INTEGER(indexstarts)[i]-1;
			stop=INTEGER(indexstarts)[i+1]-2;
			ni=stop-start+1;
		}
		/* If on the last set or if only one set */;
		if(i == T-1)
		{
			if(T==1) start=0;
			else start=INTEGER(indexstarts)[i]-1;
			stop=N-1;
			ni=stop-start+1;
		}

		if(monitor > 2) Rprintf("Tracking loop\n\ti is %d\n\tstart is %d\n\tstop is %d\n\tni is %d\n",i,start,stop,ni);

		// Set appropriate sizes for SEXP variables
		REPROTECT(yi = allocVector(REALSXP,ni), yipi);
		REPROTECT(Xi = allocMatrix(REALSXP,ni,ni), Xipi);
		REPROTECT(coordsi = allocMatrix(REALSXP,ni,2), coordsipi);
		REPROTECT(varsi = allocVector(REALSXP,ni), varsipi);
		REPROTECT(Vi = allocMatrix(REALSXP,ni,ni), Vipi);
		REPROTECT(dVi1 = allocMatrix(REALSXP,ni,ni), dVi1pi);
		REPROTECT(dVi2 = allocMatrix(REALSXP,ni,ni), dVi2pi);
		REPROTECT(dVi3 = allocMatrix(REALSXP,ni,ni), dVi3pi);
		REPROTECT(xtvxi = allocMatrix(REALSXP,betap,betap), xtvxipi);
		REPROTECT(xtvdv1vxi = allocMatrix(REALSXP,betap,betap), xtvdv1vxipi);
		REPROTECT(xtvdv2vxi = allocMatrix(REALSXP,betap,betap), xtvdv2vxipi);
		REPROTECT(xtvdv3vxi = allocMatrix(REALSXP,betap,betap), xtvdv3vxipi);
		REPROTECT(lamcalc = allocVector(VECSXP, 5), lamcalcpi);
		REPROTECT(ssdlami = allocVector(REALSXP,1), ssdlamipi);
		REPROTECT(calc1 = allocVector(VECSXP, 5), calc1pi);
		REPROTECT(calc2 = allocVector(VECSXP, 5), calc2pi);
		REPROTECT(calc3 = allocVector(VECSXP, 5), calc3pi);
		REPROTECT(ss1a = allocVector(REALSXP,1), ss1api);
		REPROTECT(ss1b = allocVector(REALSXP,1), ss1bpi);
		REPROTECT(ss1c = allocVector(REALSXP,1), ss1cpi);
		REPROTECT(ss2a = allocVector(REALSXP,1), ss2api);
		REPROTECT(ss2b = allocVector(REALSXP,1), ss2bpi);
		REPROTECT(ss2c = allocVector(REALSXP,1), ss2cpi);
		REPROTECT(trVdVa = allocVector(REALSXP,1), trVdVapi);
		REPROTECT(trVdVb = allocVector(REALSXP,1), trVdVbpi);
		REPROTECT(trVdVc = allocVector(REALSXP,1), trVdVcpi);

		INTEGER(r1)[0] = start;
		INTEGER(r2)[0] = stop;

		for(j=0;j<ni;j++) REAL(yi)[j] = REAL(y)[start+j];
		REPROTECT(Xi = subMatrix(X,r1,r2,p1,p2), Xipi);
		// This was for old code that took whole Distance matrix as input
		// REPROTECT(Di = subMatrix(D,r1,r2,r1,r2), Dipi);
		if(monitor > 2) Rprintf("Before coordsi\n");
		REPROTECT(coordsi = subMatrix(coords,r1,r2,coordp1,coordp2), coordsipi);
		if(monitor > 2) Rprintf("After coordsi\n");

		for(j=0;j<ni;j++) REAL(varsi)[j]=REAL(vars)[start+j];
		REPROTECT(Vi = setV(rpars, model, nugget, coordsi, varsi, Dscale), Vipi);
		// Calculate derivatives based upon models
		REPROTECT(dVi1 = setdV(thetai1,rpars,model,nugget,coordsi,varsi,Dscale), dVi1pi);
		if(mod > 6 || nug == 1) REPROTECT(dVi2 = setdV(thetai2,rpars,model,nugget,coordsi,varsi,Dscale), dVi2pi);
		if(mod > 6 && nug == 1) REPROTECT(dVi3 = setdV(thetai3,rpars,model,nugget,coordsi,varsi,Dscale), dVi3pi);


		/* If using typeII calculate that derivative */;
		if(INTEGER(typeII)[0]==1)
		{
			// This is overkill for calculation, but using function anyway to get ssdlambda
			REPROTECT(lamcalc = drapostcalc(Xi,Vi,dVi1,beta,dblambda,yi,invmethod), lamcalcpi);
			REPROTECT(ssdlami = getListElement(lamcalc,"ssdbeta"),ssdlamipi);
		}

		/* Calculate sum of squares and trace */;
		REPROTECT(calc1 = drapostcalc(Xi,Vi,dVi1,beta,dbeta1,yi,invmethod), calc1pi);
		REPROTECT(trVdVa = getListElement(calc1,"trvdv"),trVdVapi);
		REPROTECT(ss1a = getListElement(calc1,"dkern"),ss1api);
		REPROTECT(ss2a = getListElement(calc1,"ssdbeta"),ss2api);

		REPROTECT(xtvxi = getListElement(calc1,"xtvx"),xtvxipi);
		REPROTECT(xtvdv1vxi = getListElement(calc1,"xtvdvvx"),xtvdv1vxipi);

		// If second derivative calculate it
		if(mod > 6 || nug == 1)
		{
			REPROTECT(calc2 = drapostcalc(Xi,Vi,dVi2,beta,dbeta2,yi,invmethod),calc2pi);
			REPROTECT(trVdVb = getListElement(calc2,"trvdv"),trVdVbpi);
			REPROTECT(ss1b = getListElement(calc2,"dkern"),ss1bpi);
			REPROTECT(ss2b = getListElement(calc2,"ssdbeta"),ss2bpi);
			REPROTECT(xtvdv2vxi = getListElement(calc2,"xtvdvvx"),xtvdv2vxipi);
		}
		// If third derivative calculate it
		if(mod > 6 && nug == 1)
		{
			REPROTECT(calc3 = drapostcalc(Xi,Vi,dVi3,beta,dbeta3,yi,invmethod),calc3pi);
			REPROTECT(trVdVc = getListElement(calc3,"trvdv"),trVdVcpi);
			REPROTECT(ss1c = getListElement(calc3,"dkern"),ss1cpi);
			REPROTECT(ss2c = getListElement(calc3,"ssdbeta"),ss2cpi);
			REPROTECT(xtvdv3vxi = getListElement(calc3,"xtvdvvx"),xtvdv3vxipi);
		}

		/* Assign these values to the vector across all time through T */;
		REAL(derivsa)[i] = REAL(trVdVa)[0]-REAL(ss1a)[0]-2*REAL(ss2a)[0];
		if(mod > 6 || nug == 1) REAL(derivsb)[i]=REAL(trVdVb)[0]-REAL(ss1b)[0]-2*REAL(ss2b)[0];
		if(mod > 6 && nug == 1) REAL(derivsc)[i]=REAL(trVdVc)[0]-REAL(ss1c)[0]-2*REAL(ss2c)[0];

		/* Now assign list items */;
		SET_VECTOR_ELT(xtvxs,i,xtvxi);
		SET_VECTOR_ELT(xtvdv1vxs,i,xtvdv1vxi);
		if(mod > 6 || nug == 1) SET_VECTOR_ELT(xtvdv2vxs,i,xtvdv2vxi);
		if(mod > 6 && nug == 1) SET_VECTOR_ELT(xtvdv3vxs,i,xtvdv3vxi);
		if(INTEGER(typeII)[0]==1) REAL(ssdlams)[i]=REAL(ssdlami)[0];
	}

	/* Sum up over each block */;
	REAL(deriv)[0] = 0.0; // at least one derivative
	if(INTEGER(typeII)[0]==1) REAL(deriv)[1] = 0.0; // if typeII initialize 2nd component of deriv
	for(i=0;i<T;i++)
	{
		if(INTEGER(typeII)[0]==1)
		{
			REAL(deriv)[0] += -2*REAL(ssdlams)[i];
			REAL(deriv)[1] += REAL(derivsa)[i];
		}
		else REAL(deriv)[0] += REAL(derivsa)[i];
	}
	if(mod > 6 || nug == 1)
	{
		if(INTEGER(typeII)[0]==1) REAL(deriv)[2] = 0.0; // if typeII initialize 3rd component of deriv
		else REAL(deriv)[1] = 0.0;
		for(i=0;i<T;i++)
		{
			if(INTEGER(typeII)[0]==1) REAL(deriv)[2] += REAL(derivsb)[i];
			else REAL(deriv)[1] += REAL(derivsb)[i];
		}
	}
	if(mod > 6 && nug == 1)
	{
		if(INTEGER(typeII)[0]==1) REAL(deriv)[3] = 0.0; // if typeII initialize 4th component of deriv
		else REAL(deriv)[2] = 0.0;
		for(i=0;i<T;i++)
		{
			if(INTEGER(typeII)[0]==1) REAL(deriv)[3] += REAL(derivsc)[i];
			else REAL(deriv)[2] += REAL(derivsc)[i];
		}
	}

	// Now sum up X^TV^{-1}X and X^TV^{-1}dVV^{-1}X and add calculate X^TV^{-1}VX+cSigma^{-1}
	PROTECT(sumxtvx = sumMatrixList(xtvxs));
	PROTECT(sumxtvdv1vx = sumMatrixList(xtvdv1vxs));
	if(mod > 6 || nug == 1)	PROTECT(sumxtvdv2vx = sumMatrixList(xtvdv2vxs));
	if(mod > 6 && nug == 1)	PROTECT(sumxtvdv3vx = sumMatrixList(xtvdv3vxs));

	// Now calculate the prior part of the derivative
	PROTECT(Sigmacalc = matrixcalc(Sigma,invmethod));
	PROTECT(Sigmainv = getListElement(Sigmacalc,"Vinv"));
	PROTECT(cSiginv = allocMatrix(REALSXP,betap,betap));
	for(j=0;j<betap;j++)
	{
		for(i=0;i<betap;i++)
		{
			REAL(cSiginv)[i+j*betap] = REAL(rlambda)[0]*REAL(Sigmainv)[i+j*betap];
		}
	}
	PROTECT(xtvxpluscSiginv = XplusY(sumxtvx,cSiginv));
	PROTECT(xtvxcSiginv = cholinvV(xtvxpluscSiginv));
	PROTECT(xtvxcSiginvdv1 = XtimesY(xtvxcSiginv,sumxtvdv1vx));
	PROTECT(trxtvdv1 = tr(xtvxcSiginvdv1));
	if(mod > 6 || nug == 1)
	{
		PROTECT(xtvxcSiginvdv2 = XtimesY(xtvxcSiginv,sumxtvdv2vx));
		PROTECT(trxtvdv2 = tr(xtvxcSiginvdv2));
	}
	if(mod > 6 && nug == 1)
	{
		PROTECT(xtvxcSiginvdv3 = XtimesY(xtvxcSiginv,sumxtvdv3vx));
		PROTECT(trxtvdv3 = tr(xtvxcSiginvdv3));
	}

	PROTECT(dpriorcalc1 = XminusY(beta,mu));
	PROTECT(dpriorcalc2 = tX(dpriorcalc1));
	PROTECT(dpriorcalc3 = XtimesY(dpriorcalc2,Sigmainv));
	PROTECT(dprior1 = XtimesY(dpriorcalc3,dbeta1));
	if(mod > 6 || nug == 1) PROTECT(dprior2 = XtimesY(dpriorcalc3,dbeta2));
	if(mod > 6 && nug == 1) PROTECT(dprior3 = XtimesY(dpriorcalc3,dbeta3));

	// Extra calculations when using Type II
	PROTECT(dlam1 = XtimesY(dpriorcalc3,dpriorcalc1));
	PROTECT(dlam2 = XtimesY(xtvxcSiginv,Sigmainv));
	PROTECT(trdlam = tr(dlam2));
	if(INTEGER(typeII)[0]==1)
	{
		PROTECT(dlam3 = XtimesY(dpriorcalc3,dblambda));
	}
	else PROTECT(dlam3 = allocVector(REALSXP,1)); // Setup dummy protect so as to keep PROTECTS the same


	// Add other parts of derivative (the second trxtvdv's and dprior)
	if(INTEGER(typeII)[0]==0)
	{
		REAL(deriv)[0] = REAL(deriv)[0] - REAL(trxtvdv1)[0] + 2 * REAL(rlambda)[0] * REAL(dprior1)[0];
		if(mod > 6 || nug == 1) REAL(deriv)[1] = REAL(deriv)[1] - REAL(trxtvdv2)[0] + 2 * REAL(rlambda)[0] * REAL(dprior2)[0];
		if(mod > 6 && nug == 1) REAL(deriv)[2] = REAL(deriv)[2] - REAL(trxtvdv3)[0] + 2 * REAL(rlambda)[0] * REAL(dprior3)[0];
	}
	if(INTEGER(typeII)[0]==1)
	{
		REAL(deriv)[0] = REAL(deriv)[0] + REAL(trdlam)[0] + REAL(dlam1)[0] + 2 * REAL(rlambda)[0] * REAL(dlam3)[0] - (betap / REAL(rlambda)[0]);
		REAL(deriv)[1] = REAL(deriv)[1] - REAL(trxtvdv1)[0] + 2 * REAL(rlambda)[0] * REAL(dprior1)[0];
		if(mod > 6 || nug == 1) REAL(deriv)[2] = REAL(deriv)[2] - REAL(trxtvdv2)[0] + 2 * REAL(rlambda)[0] * REAL(dprior2)[0];
		if(mod > 6 && nug == 1) REAL(deriv)[3] = REAL(deriv)[3] - REAL(trxtvdv3)[0] + 2 * REAL(rlambda)[0] * REAL(dprior3)[0];
	}

	// Different unprotects due to dprior1 - dprior3
	if(spatp == 3) UNPROTECT(81);
	if(spatp == 2) UNPROTECT(77);
	if(spatp == 1) UNPROTECT(73);
	return(deriv);
}

