#include "bjl.h"

/*********************************************************
*   This is C code to set the covariance matrix 	 *
*********************************************************/

// d is the distance matrix
// pars is the vector of spatial parameters
// model is the spatial model chosen

SEXP dspatCOV(SEXP thetai, SEXP D, SEXP pars, SEXP model, SEXP nugget)
{
	SEXP dV, DIMS;
	int i, j, m, n, npar, mod, nug;
	double dvar, par1, par2, par3, nugpar; 

	PROTECT(DIMS = getAttrib(D, R_DimSymbol));
	npar = LENGTH(pars);
	m = INTEGER(DIMS)[0];
	n = INTEGER(DIMS)[1];
	if(m != n) error("The distance matrix must be square");

	mod = INTEGER(model)[0];
	nug = INTEGER(nugget)[0];

	if(mod <= 6) //One parameter models 
	{
		if(npar != 2 && nug == 1) error("spatial parameters length is wrong");
		if(npar != 1 && nug == 0) error("spatial parameters length is wrong");
		par1 = REAL(pars)[0];
		if(nug==1) nugpar = REAL(pars)[1];
		else nugpar = 0.0;
		par2 = 0.0; par3 = 0.0;
	}
	if(mod <= 9 && mod > 6) //Two parameter models
	{
		if(npar != 3 && nug == 1) error("spatial parameters length is wrong");
		if(npar != 2 && nug == 0) error("spatial parameters length is wrong");
		par1 = REAL(pars)[0];
		par2 = REAL(pars)[1];
		if(nug==1) nugpar = REAL(pars)[2];
		else nugpar = 0.0;
		par3 = 0.0;
	}

	PROTECT(dV = allocMatrix(REALSXP, m, n));

	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			// This puts the nugget:sill ratio in off-diagonals instead of diagonal
			if(nug==1)
			{
				// Only works on a few models
				if(mod == 1 || mod == 2 || mod ==4 || mod == 5 || mod == 7)
				{
					if(INTEGER(thetai)[0] == npar)
					{
						// Use spatvar from spatCOV.c
						dvar = -spatvar(par1,par2,par3,REAL(D)[i*n+j],mod);
					}
					else
					{
						dvar = (1-nugpar) * dspatvar(INTEGER(thetai)[0],par1,par2,par3,REAL(D)[i*n+j],mod);
					}
				}
				else
				{
					error("Not sure how to handle nugget here\n");
					//var = spatvar(par1,par2,par3,REAL(D)[i*n+j],mod)/(1-nugpar);
				}
			}
			else dvar = dspatvar(INTEGER(thetai)[0],par1,par2,par3,REAL(D)[i*n+j],mod);
			REAL(dV)[i*n+j] = dvar;
			dvar = 0.0;

			// // This puts nugget:sill ratio only in diagonal components
			// var = spatvar(par1,par2,par3,REAL(D)[i*n+j],mod);
			// REAL(V)[i*n+j] = var;
			// var = 0.0;
		}
	}
	/*fix diagonal entries*/
	for(i=0;i<n;i++)
	{
		// This puts the nugget:sill ratio in off-diagonals instead of diagonal
		REAL(dV)[i*n+i] = 0.0;

		// // This puts nugget:sill ratio only in diagonal components
		// if(nug==1) REAL(V)[i*n+i] = 1.0 / (1 - nugpar);
		// else REAL(V)[i*n+i] = 1.0;
	}

	UNPROTECT(2);
	return(dV);
}

/* To calculate the spatial correlation, based upon geoR and RLS notes */;
double dspatvar(int thetai, double par1, double par2, double par3, double dij, int model)
{
	double dvij;
	
	/* Note that one parameter models don't care about i*/;
	// Exponential
	if(model == 1)
	{
		if(dij/par1 <= 20)
		{
			dvij = dij*exp(-dij/par1)/R_pow(par1,2);
		}
		else dvij = 0.0;
	}
	// Gaussian
	if(model == 2)
	{
		dvij = 2*R_pow(dij,2)*exp(-(dij*dij)/(par1*par1))/R_pow(par1,3);
	}
	// Cubic
	if(model == 3)
	{
		if(dij >= par1) dvij = 0.0;
		else dvij = -14*R_pow(dij,2)/R_pow(par1,3) + 26.25*R_pow(dij,3)/R_pow(par1,4) + 5.25*R_pow(dij,7)/R_pow(par1,8);
	}
	// Spherical
	if(model == 4)
	{
		if(dij >= par1) dvij = 0.0;
		else dvij = 3*dij/(2*R_pow(par1,2)) - 3*R_pow(dij,3)/(2*R_pow(par1,4));
	}
	// Wave
	if (model == 5)
	{
		dvij = sin(dij/par1)/dij - cos(dij/par1)/par1;
	}
	// Linear (can't figure out what this should because not stationary
	if (model == 6)
	{
		error("This model is not working yet\n");
		dvij = 0.0;
	}

	/* Two parameter models */;

	// Matern
	if(model == 7)
	{
		// RLS Course Notes version:
		if(thetai == 1)
		{
			if(par2 < 50)
			{
				dvij = dmatern1(dij,par1,par2);
			}
			// Use Gaussian if par2 gets too big
			else dvij = 2*R_pow(dij,2)*exp(-(dij*dij)/(par1*par1))/R_pow(par1,3);
		}
		if(thetai == 2)
		{
			if(par2 < 50)
			{
				dvij = dmatern2(dij,par1,par2);
			}
			// Use Gaussian if par2 gets too big
			else dvij = 0.0;
		}
	}
	// cauchy
	if(model == 8)
	{
		if(thetai == 1)
		{
			dvij = -2*par2*R_pow(dij,2)*R_pow(1+R_pow(dij/par1,2),par2-1)/R_pow(par1,3);
		}
		else
		{
			dvij = log(1+R_pow(dij/par1,2))*R_pow(1+R_pow(dij/par1,2),par2);
		}
	}
	// exponential power
	if(model == 9)
	{
		if(thetai == 1)
		{
			dvij = par2*R_pow(dij/par1,par2)*exp(-R_pow(dij/par1,par2))/par1;
		}
		else
		{
			dvij = -R_pow(dij/par1,par2)*log(dij/par1)*exp(-R_pow(dij/par1,par2));
		}
	}

	return(dvij);
}

double dmatern1(double d, double par1, double par2)
{
	double the, bes, con, pt1, pt2, res;

	the = 2*d*sqrt(par2)/par1;
	bes = bessel_k(the,par2,1);
	con = 2*d*R_pow(the/2,par2)*sqrt(par2)/(R_pow(par1,2)*gammafn(par2));

	pt1 = bessel_k(the,par2-1,1)+bessel_k(the,par2+1,1);
	pt2 = par2*bes*2/the;

	res = con*(pt1-pt2);
	return(res);
}

double dmatern2(double d, double par1, double par2)
{
	double the, bes, con, pt1, pt2, pt3, res;
	double delta=1e-6, dK2;

	the = 2*d*sqrt(par2)/par1;
	bes = bessel_k(the,par2,1);
	con = 2*R_pow(the/2,par2)/gammafn(par2);

	// Approximate derivative of BesselK function
	dK2 = (bessel_k(the,par2+delta,1)-bes)/delta;

	pt1 = bes*(.5+log(the/2));
	pt2 = bes*psigamma(par2,0);
	pt3 = d*(-bessel_k(the,par2-1,1)-bessel_k(the,par2+1,1))/
		(2*par1*sqrt(par2))+dK2;

//	Rprintf("con is %d, dK2 is %d, pt1 is %d, pt3 is\n");

	res = con*(pt1-pt2+pt3);
	return(res);
}

