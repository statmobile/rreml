#include "bjl.h"

/* C function that calls LAPACK libraries in R to invert a covariance
   matrix (positive definite) by taking the cholesky decomposition */


void COVinv(double *v, int *n)
{
	double *Vup;
	int i, j, ind=0, status, nv=*n;

	Vup=(double *) R_alloc(nv*nv, sizeof(double));

	/*Get upper part of matrix for Cholesky Decomposition
	  and zero out the bottom triangle to pass to Fortran 
	  routines dpotrf and dpotri*/
	for(i=0;i<nv;i++)
	{
		for(j=0;j<nv;j++)
		{
			if(j<=i)
			{
				Vup[ind]=v[nv*i+j];
			}
			else
			{
				Vup[ind]=0.0;
			}
			ind=ind+1;
		}
	}
	/* Need error catching for the Fortran codes */
	F77_CALL(dpotrf)("Upper", &nv, Vup, &nv, &status); //Take cholesky decomp
	if(status!=0) Rprintf("Possible problem with cholesky");
	F77_CALL(dpotri)("Upper", &nv, Vup, &nv, &status); //Take derivative of cholesky
	if(status!=0) Rprintf("Possible problem with cholesky derivative");

	/* Now write inverse to v */
	for(j=0;j<nv;j++)
	{
		for(i=0;i<=j;i++)
		{
			v[i+j*nv]=Vup[i+j*nv];
		}
	}
	/* And map the bottom part of the symmetric matrix */
	for(j=0;j<nv;j++)
	{
		for(i=0;i<j;i++)
		{
			v[j+i*nv]=Vup[i+j*nv];
		}
	}
}



