#include "bjl.h"

/* This is to generate the log likelihood for multivariate Gaussian
   distribution */


//SEXP GAUSSlkhd(SEXP lkhd, SEXP pars, SEXP y, SEXP X, SEXP D, SEXP model)
SEXP learnC(SEXP lkhd, SEXP pars, SEXP y, SEXP X, SEXP D, SEXP model)
{
	SEXP TESTMatrix, B, TOL; //Test variables
	SEXP DIMS;
	int N, P, i, j;

	DIMS = getAttrib(X,R_DimSymbol);
	N = INTEGER(DIMS)[0]; // Rows of X
	P = INTEGER(DIMS)[1]; // Columns of X

	// // Setup test return matrix
	// PROTECT(TESTMatrix = allocMatrix(INTSXP,N,P));
	// for(j = 0; j < P; j++)
	// {
	// 	for(i = 0; i < N; i++)
	// 	{
	// 		INTEGER(TESTMatrix)[j*N+i] = j*N+i;
	// 	}
	// }


	/* Now test the transpose of a matrix and multiplication */
	PROTECT(TESTMatrix = allocMatrix(REALSXP,P,P));
	PROTECT(B = allocMatrix(REALSXP,P,P));
	PROTECT(TOL = allocVector(REALSXP,1));

	for(j=0;j<P;j++) for(i=0;i<P;i++) REAL(B)[j*P+i]=0.0;
	for(j=0;j<P;j++) REAL(B)[j*P+j]=1.0;
	REAL(TOL)[0] = 1e-07; // Standard tolerance used in R
            
	TESTMatrix = bjlLa_dgesv(XtimesY(tX(X),X),B,TOL);


	UNPROTECT(3);
	return(TESTMatrix);


}



		

	       
