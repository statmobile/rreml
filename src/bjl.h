#include <R.h>
#include <Rmath.h>
#include <Rinternals.h>
#include <R_ext/Lapack.h>
#include <R_ext/RS.h>

/* My C functions to be used in libraries */

/***********************************************************
 *
 * Calculate the aposterior distribution
 * and its derivative

 * There are various other functions used within apost
 * and dapost for this
 *
***********************************************************/
SEXP aposteriori(SEXP pars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, SEXP vars, SEXP model, 
		 SEXP indexstarts, SEXP mu, SEXP Sigma, SEXP BGLS, SEXP nug, SEXP invmethod);
SEXP daposteriori(SEXP pars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
		  SEXP vars, SEXP model, SEXP indexstarts, SEXP mu, 
		  SEXP Sigma, SEXP BGLS, SEXP nugget, SEXP invmethod);

/* Ridge version of aposteriori and Bayesian Generalized Least Squares */
SEXP raposteriori(SEXP pars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
		  SEXP vars, SEXP model, SEXP indexstarts, SEXP mu, 
		  SEXP Sigma, SEXP nugget, SEXP invmethod, SEXP typeII);
SEXP draposteriori(SEXP pars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
		   SEXP vars, SEXP model, SEXP indexstarts, SEXP mu, SEXP typeII, 
		   SEXP Sigma, SEXP nugget, SEXP invmethod);
SEXP rbgls(SEXP spatpars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
	   SEXP vars, SEXP mu, SEXP Sigma, SEXP model, SEXP indexstarts, 
	   SEXP nugget, SEXP invmethod, SEXP typeII);
SEXP drbgls(SEXP spatpars, SEXP lambda, SEXP y, SEXP X, SEXP coords, SEXP Dscale, 
	    SEXP vars, SEXP mu, SEXP Sigma, SEXP model, SEXP indexstarts, 
	    SEXP nugget, SEXP invmethod, SEXP typeII);


/* Functions for calculating the spatial covariances */
SEXP setV(SEXP spatpars, SEXP model, SEXP nugget, SEXP coords, 
	  SEXP vars, SEXP Dscale);
SEXP setdV(SEXP di, SEXP spatpars, SEXP model, SEXP nugget, SEXP coords, 
	   SEXP vars, SEXP Dscale);
SEXP spatCOV(SEXP D, SEXP pars, SEXP model, SEXP nug);
SEXP dspatCOV(SEXP i, SEXP D, SEXP pars, SEXP model, SEXP nug);
double spatvar(double par1, double par2, double par3, double dij, int model);
double dspatvar(int i, double par1, double par2, double par3, double dij, int model);
double dmatern1(double d, double par1, double par2);
double dmatern2(double d, double par1, double par2);

/* Setting geodesic distance matrix */
SEXP geodist(SEXP x, SEXP y);
double dist(double lon1, double lat1, double lon2, double lat2);

/* Calculate parts of Gaussian likelihood (See mvnorm.c) */
SEXP mvnorm(SEXP y, SEXP X, SEXP beta, SEXP V, SEXP invmethod);
SEXP dmvnorm(SEXP y, SEXP X, SEXP beta, SEXP V, SEXP dV, SEXP invmethod);
SEXP dapostcalc(SEXP X, SEXP V, SEXP dV, SEXP beta, SEXP dbeta, SEXP y, SEXP invmethod);
SEXP dbglscalc(SEXP X, SEXP V, SEXP dV, SEXP y, SEXP invmethod);
SEXP bglscalc(SEXP X, SEXP V, SEXP y, SEXP invmethod);
SEXP rapostcalc(SEXP y, SEXP X, SEXP beta, SEXP V, SEXP invmethod);
SEXP drapostcalc(SEXP X, SEXP V, SEXP dV, SEXP beta, SEXP dbeta, SEXP y, SEXP invmethod);



/***********************************************************
 *
 * Calculate the Bayesian Generalized Least Squares estimate
 *
***********************************************************/
SEXP bgls(SEXP spatpars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, SEXP vars, SEXP mu,
	  SEXP sa, SEXP model, SEXP indexstarts, SEXP nug, SEXP invmethod);
SEXP dbgls(SEXP spatpars, SEXP y, SEXP X, SEXP coords, SEXP Dscale, SEXP vars, SEXP mu,
	  SEXP sa, SEXP model, SEXP indexstarts, SEXP nug, SEXP invmethod);

/***********************************************************
 *
 * SEXP function to call Lapack routines
 * Should rename from bjlLa* to SEXPLa*
 *
 * Maybe also put into SEXPalgebra?
 *
***********************************************************/
/* My calls to the Lapack routines */
SEXP bjlLa_chol(SEXP A);
SEXP bjlLa_chol2inv(SEXP A, SEXP size);
SEXP bjldet_ge_real(SEXP Ain, SEXP logarithm);
SEXP bjlLa_dgesv(SEXP A, SEXP Bin, SEXP tolin);
SEXP bjlLa_svd(SEXP jobu, SEXP jobv, SEXP x, SEXP s, SEXP u, SEXP v, SEXP method);
SEXP bjlLa_rs(SEXP xin, SEXP only_values);


/***********************************************************
 *
 * Functions for all of my linear SEXP linear algebra 
 *
 * These functions are in SEXPalgebra.c
 *
***********************************************************/

/* If X is not a matrix, convert it from vector to matrix */
SEXP vec2mat(SEXP X);
/* get the list element named str, or return NULL */
SEXP getListElement(SEXP list, char *str);
/* (nx x px) X times (ny x py) Y = (nx x py) Z */
SEXP XtimesY(SEXP X, SEXP Y);
/* (nx x px) X^T times (ny x py) Y = (px x py) Z */
SEXP XTtimesY(SEXP X, SEXP Y); // WARNING: Potential Problems
/* (nx x px) X - (ny x py) Y = (nx x py) Z */
SEXP XminusY(SEXP X, SEXP Y);
/* (nx x px) X + (ny x py) Y = (nx x py) Z */
SEXP XplusY(SEXP X, SEXP Y);
/* Calculate the trace of X */
SEXP tr(SEXP X);
/* transpose of (nx x px) X */
SEXP tX(SEXP X);
/* Calculate determinant of a matrix (Good reference for dealing with lists) */
SEXP detX(SEXP X);
/* Using cholesky decomposition, calculate V^{-1} */
SEXP cholinvV(SEXP V);
/* Calculate SVD of square matrix A = UDV^T give D, U and V^T */
SEXP svdsqV(SEXP A);
/* Using SVD, calculate A^{-1} */
SEXP svdinvV(SEXP A);
/* Caclulate spectral decomposition inverse of symmetric square matrix A = UDU^T */
SEXP spectinvV(SEXP A);
/* Special program to calculate the inverse and log absolute value determinant all in one call
   method=0 uses Cholesky, method=1 uses spectral decomposition,  method=2 uses SVD */
SEXP matrixcalc(SEXP V, SEXP method);
/* Generic inverse of a matrix using LAPACK routine */
SEXP Xinv(SEXP X);
/* C code to get X[r1:r2,c1:c2] */
SEXP subMatrix(SEXP X, SEXP r1, SEXP r2, SEXP c1, SEXP c2);
/* Code to sum across list of vectors */
SEXP sumVectorList(SEXP veclist);
/* Code to sum across list of matrices */
SEXP sumMatrixList(SEXP matlist);




/***********************************************************
 *
 * Random functions I may find handy in the future
 *
***********************************************************/

// To compute inverse of positive definite covariance matrix v
void COVinv(double *v, int *n);

